# Kcoy Game Engine

## Dependencies

### [spdlog](https://github.com/gabime/spdlog)
### [sfml](https://www.sfml-dev.org/index.php)
### [chipmunk2D](https://github.com/slembcke/Chipmunk2D)

## SetUp
1. Run *premake.bat*
2. Go under *kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk*
3. Open the project *chipmunk.vcxproj*
4. Build *Debug DLL* and *Release DLL* for 64bit architecture
5. Go under *kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64*
6. Rename both directories by removing the space.
   - From Debug DLL to DebugDLL
   - From Release DLL to ReleaseDLL
7. Go to the root directory
8. Open the solution
9. Set *zelda_clone* as startup project

## Current Architecture
```
  +------+                    +    +------+
  |      |                    |    |      |
  | Core |                    |    | Game |
  |      |                    |    |      |
  +------+                    |    +------+
                              |
  +-----+                     |
  |Start|                     |
  +--+--+                     |
     |                        |
  +--+---------+              |
  |EventHandler|              |
  +------------+              |
+---------------------------------------------------------------+
|    |                        |                                 |
| +--+------+                 |                                 |
| |GameLogic|                 |   +--------------------------+  |
| +--+----+-+    +------+     |   |GameObject(and components)|  |
|    |    +------+Update|     |   +--------------------------+  |
|    |           +------+     |                                 |
|    |                        |                                 |
+---------------------------------------------------------------+
  +-------------+             |
  |PhysicsUpdate|             |
  +--+----+-----+             |
     |    |     +-----------+ |
     |    +-----+ Collision | |
     |          | Callbacks | |
     |          +-----------+ |
     |                        |
  +--+---+                    |
  |Render|                    |
  +------+                    |
                              +
```

## TODO:
- Create build script
- **[DONE]** Demo1: Create a Character that moves left to right
    - Create Transform component
    - Create Render that renders all gameobjects base on the transform
- **[DONE]** Demo2: Move Character by forces and detect phantom collisions
    - Add physics to the engine
    - Create Rigidbody
    - Detect collisions
- Demo3: FlappyBird
    - Add velocity
    - Add Transform global and local position(maybe Transform matrix)
- Demo4: Zelda Dungeon
