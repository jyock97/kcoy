workspace "kcoy"

    architecture "x86_64"
	
    configurations {
	    "Debug",
	    "Release",
	    "Dist"
    }

outputDir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"
projectName = "zelda_clone"


include "kcoy"
include "zelda_clone"