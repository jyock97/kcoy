#include <Kcoy.h>
#include <kcoy/EntryPoint.h>
#include "demo1/Demo1.h"
#include "demo2/Demo2.h"
#include "demo3/Demo3.h"

class ZeldaCloneApp : public Kcoy::Application {
public:
    Demo1 *demo1;
    Demo2 *demo2;
    Demo3 *demo3;

    ZeldaCloneApp() {}
    ~ZeldaCloneApp() {}
    
private:
    void start () {
        Kcoy::Logger::ClientLogger()->info("Application start");
        //demo1 = new Demo1();
        //demo2 = new Demo2();
        demo3 = new Demo3();
    }

    void end() {
        //delete demo1;
        //delete demo2;
        delete demo3;
    }
};

Kcoy::Application *Kcoy::createApplication() {
    return new ZeldaCloneApp();
}
