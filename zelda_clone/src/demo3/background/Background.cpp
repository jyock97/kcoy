#include "Background.h"
#include "demo3/Demo3.h"

Background::Background(Demo3 *controllerRef) : Kcoy::GameObject("Background")
{
    this->controllerRef = controllerRef;
    this->backgroundTile1 = new BackgroundTile();
    this->backgroundTile2 = new BackgroundTile();

    this->reset();
}
Background::~Background()
{
    delete(this->backgroundTile2);
    delete(this->backgroundTile1);
}

void Background::update()
{
    if (this->controllerRef->getIsGameStarted()) {
        float moveSpeed = 3;
        const int arraySize = 2;
        BackgroundTile *backgroundTiles[arraySize] = { backgroundTile1, backgroundTile2 };
        for (int i = 0; i < arraySize; i++) {
            Kcoy::Vector2 pos = backgroundTiles[i]->transform.getPosition();
            pos += Kcoy::Vector2(-1, 0) * moveSpeed * Kcoy::Time::deltaTime();
            if (pos.getX() < -28.5) {
                pos += Kcoy::Vector2(56, 0);
            }
            backgroundTiles[i]->transform.setLocalPosition(pos);
        }
    }
}

void Background::reset()
{
    this->backgroundTile1->transform.setLocalPosition(Kcoy::Vector2(0, -1));
    this->backgroundTile2->transform.setLocalPosition(Kcoy::Vector2(28, -1));
}
