#pragma once
#include "Kcoy.h"

class BackgroundTile : public Kcoy::GameObject
{
public:
    BackgroundTile();
    ~BackgroundTile();

    Kcoy::SpriteRender *spriteRender;
    Kcoy::RigidBody *rigidBody;

    void update();
};

