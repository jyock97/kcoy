#include "BackgroundTile.h"

BackgroundTile::BackgroundTile() : Kcoy::GameObject("BackgroundTile")
{
    this->spriteRender = new Kcoy::SpriteRender(&this->transform, "assets/sprites/background/stars.png", 1, 4, Kcoy::SpriteOrigin::CenterLeft);
}


BackgroundTile::~BackgroundTile()
{
    delete(this->spriteRender);
}

void BackgroundTile::update() { }
