#pragma once
#include "Kcoy.h"
#include "BackgroundTile.h"

class Demo3;
class Background : public Kcoy::GameObject
{
public:
    Background(Demo3 *controllerRef);
    ~Background();

    BackgroundTile *backgroundTile1;
    BackgroundTile *backgroundTile2;
    void update();
    void reset();

private:
    Demo3 *controllerRef;
};

