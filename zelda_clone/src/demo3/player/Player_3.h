#pragma once
#include "Kcoy.h"

// TODO redesign this to avoid cycle inclution
class Demo3;
class Score;
class Player_3 : public Kcoy::GameObject
{
public:
    Player_3(Demo3 *controllerRef, Score *score);
    ~Player_3();
    
    Kcoy::SpriteRender *spriteRenderer;
    Kcoy::RigidBody *rigidBody;
    Kcoy::BoxCollider *boxCollider;

    void update();
    void reset();

private:
    Demo3 *controllerRef;
};

