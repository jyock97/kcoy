#include "Player_3.h"
#include "demo3/Demo3.h"
#include "demo3/score/Score.h"

Player_3::Player_3(Demo3 *controllerRef, Score *score) : Kcoy::GameObject("TopObstacle")
{
    this->controllerRef = controllerRef;
    this->spriteRenderer = new Kcoy::SpriteRender(&this->transform, "assets/sprites/ship/ship.png", 0, 32, Kcoy::SpriteOrigin::Center);

    this->rigidBody = new Kcoy::RigidBody(&this->transform, false);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidBody, false, 0.6f, 0.38f);
    this->reset();

    this->boxCollider->addCollisionStartCallback([&, controllerRef](Kcoy::GameObject *gameObject) {
        Kcoy::Logger::ClientLogger()->info("Collision End Detected with {}", (void*)gameObject);
        if (gameObject->name.compare("CheckPoint")) {
            controllerRef->restartGame();
        }
    });
    this->boxCollider->addCollisionEndCallback([&, score](Kcoy::GameObject *gameObject) {
        Kcoy::Logger::ClientLogger()->info("Collision End Detected with {}", (void*)gameObject);
        if (!gameObject->name.compare("CheckPoint")) {
            score->scoreUp();
        }
    });
}


Player_3::~Player_3()
{
    delete this->spriteRenderer;
}

void Player_3::update()
{
    if (!this->controllerRef->getIsGameStarted()) {
        this->transform.setLocalPosition(Kcoy::Vector2(2, -1));
        this->rigidBody->setVelocity(Kcoy::Vector2(0, 0));
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::Space)) {
        this->rigidBody->setVelocity(Kcoy::Vector2(0, 2));
    }
}

void Player_3::reset()
{
    this->transform.setLocalPosition(Kcoy::Vector2(2, -1));
}
