#pragma once
#include "Kcoy.h"
#include "demo3/score/Score.h"
#include "demo3/player/Player_3.h"
#include "demo3/background/Background.h"
#include "demo3/obstacle/ObstacleController.h"

// Controller class
class Demo3 : public Kcoy::GameObject {
public:
    Demo3();
    ~Demo3();

    Score *score;
    Background *background;
    ObstacleController *obstacleController;
    Player_3 *player;

    void update();

    bool getIsGameStarted();
    void startGame();
    void restartGame();

private:
    bool isGameStarted = false;
};

