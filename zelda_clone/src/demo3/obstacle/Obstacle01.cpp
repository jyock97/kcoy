#include "Obstacle01.h"

Obstacle01::Obstacle01(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 pos) :
    Kcoy::GameObject("Obstacle01", parentTransformRef)
{
    this->spriteRender = new Kcoy::SpriteRender(&this->transform, "assets/sprites/obstacle/obstacle01.png", 5, 32);
    this->transform.setLocalPosition(pos);
}


Obstacle01::~Obstacle01()
{
    delete(this->spriteRender);
}

void Obstacle01::update() { }
