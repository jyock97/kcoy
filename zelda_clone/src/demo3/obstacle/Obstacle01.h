#pragma once
#include "Kcoy.h"
class Obstacle01 : public Kcoy::GameObject
{
public:
    Obstacle01(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 pos);
    ~Obstacle01();

    Kcoy::SpriteRender *spriteRender;

    void update();
};

