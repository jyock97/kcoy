#pragma once
#include "Kcoy.h"

class Border : public Kcoy::GameObject
{
public:
    Border();
    ~Border();

    Kcoy::RigidBody *rigidbody;
    Kcoy::BoxCollider *boxCollider;

    void update();
};

