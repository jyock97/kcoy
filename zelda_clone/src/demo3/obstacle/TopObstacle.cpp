#include "TopObstacle.h"

TopObstacle::TopObstacle() : Kcoy::GameObject("TopObstacle")
{
    this->obstacle02_1 = new Obstacle02(&this->transform, Kcoy::Vector2(0, 0.75));
    this->obstacle02_2 = new Obstacle02(&this->transform, Kcoy::Vector2(0, 0.25));
    this->obstacle02_3 = new Obstacle02(&this->transform, Kcoy::Vector2(0, -0.25));

    this->obstacle01_1 = new Obstacle01(&this->transform, Kcoy::Vector2(0, -0.75));

    this->rigidbody = new Kcoy::RigidBody(&this->transform, true);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidbody, false, 0.5f, 2);
}

TopObstacle::~TopObstacle()
{
}

void TopObstacle::update() {

}