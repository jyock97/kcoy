#pragma once
#include "Kcoy.h"
#include "Obstacle01.h"
#include "Obstacle02.h"

class TopObstacle : public Kcoy::GameObject
{
public:
    TopObstacle();
    ~TopObstacle();

    Obstacle01 *obstacle01_1;

    Obstacle02 *obstacle02_1;
    Obstacle02 *obstacle02_2;
    Obstacle02 *obstacle02_3;

    Kcoy::RigidBody *rigidbody;
    Kcoy::BoxCollider *boxCollider;

    void update();
};

