#include "ObstacleController.h"
#include "demo3/Demo3.h"
#include <stdlib.h> // TODO move this into Kcoy::Random
#include <time.h> // TODO move this into Kcoy::Random

ObstacleController::ObstacleController(Demo3 *controllerRef) :
    Kcoy::GameObject("ObstacleController")
{
    srand(time(0));
    this->controllerRef = controllerRef;
    
    this->b1 = new Border();
    this->b2 = new Border();
    this->b1->transform.setLocalPosition(Kcoy::Vector2(2.5, 0));
    this->b2->transform.setLocalPosition(Kcoy::Vector2(2.5, -2.8));

    this->to1 = new TopObstacle();
    this->to2 = new TopObstacle();
    this->to3 = new TopObstacle();
    this->to4 = new TopObstacle();

    this->bo1 = new BottomObstacle();
    this->bo2 = new BottomObstacle();
    this->bo3 = new BottomObstacle();
    this->bo4 = new BottomObstacle();

    this->cp1 = new CheckPoint();
    this->cp2 = new CheckPoint();
    this->cp3 = new CheckPoint();
    this->cp4 = new CheckPoint();

    this->reset();
}

ObstacleController::~ObstacleController()
{ 
    delete this->cp4;
    delete this->cp3;
    delete this->cp2;
    delete this->cp1;
    delete this->bo4;
    delete this->bo3;
    delete this->bo2;
    delete this->bo1;
    delete this->to4;
    delete this->to3;
    delete this->to2;
    delete this->to1;
}

void ObstacleController::update()
{
    if (this->controllerRef->getIsGameStarted()) {
        float moveSpeed = 2;
        Kcoy::GameObject *topObstacles[4] = { to1, to2, to3, to4 };
        Kcoy::GameObject *bottomObstacles[4] = { bo1, bo2, bo3, bo4 };
        Kcoy::GameObject *checkpoints[4] = { cp1, cp2, cp3, cp4 };

        for (int i = 0; i < 4; i++) {
            Kcoy::Vector2 pos = topObstacles[i]->transform.getPosition();
            pos += Kcoy::Vector2(-1, 0) * moveSpeed * Kcoy::Time::deltaTime();
            if (pos.getX() < -0.5) {
                pos += Kcoy::Vector2(8, 0);
                float newYPosition = this->getRandomYOffset(topObstacles[(i + 1) % 4]);
                pos.setY(newYPosition);
            }
            topObstacles[i]->transform.setLocalPosition(pos);
            bottomObstacles[i]->transform.setLocalPosition(pos - Kcoy::Vector2(0, 3));
            checkpoints[i]->transform.setLocalPosition(pos + Kcoy::Vector2(0.3, -1.5));
        }
    }
}

void ObstacleController::reset()
{
    to1->transform.setLocalPosition(Kcoy::Vector2(4, 0));
    to2->transform.setLocalPosition(Kcoy::Vector2(6, 0));
    to3->transform.setLocalPosition(Kcoy::Vector2(8, 0));
    to4->transform.setLocalPosition(Kcoy::Vector2(10, 0));
    bo1->transform.setLocalPosition(Kcoy::Vector2(4, -3));
    bo2->transform.setLocalPosition(Kcoy::Vector2(6, -3));
    bo3->transform.setLocalPosition(Kcoy::Vector2(8, -3));
    bo4->transform.setLocalPosition(Kcoy::Vector2(10, -3));
    cp1->transform.setLocalPosition(Kcoy::Vector2(4.3, -1.5));
    cp2->transform.setLocalPosition(Kcoy::Vector2(6.3, -1.5));
    cp3->transform.setLocalPosition(Kcoy::Vector2(8.3, -1.5));
    cp4->transform.setLocalPosition(Kcoy::Vector2(10.3, -1.5));
}

float ObstacleController::getRandomYOffset(Kcoy::GameObject *obj)
{
    float random = rand() % 6;
    random -= 3;
    random /= 10;
    random += obj->transform.getPosition().getY();
    if (random < -0.5) {
        random = -0.5;
    }
    else if (random > 1) {
        random = 1;
    }
    return random;
}
