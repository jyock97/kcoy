#pragma once
#include "Kcoy.h"
class Obstacle02 : public Kcoy::GameObject
{
public:
    Obstacle02(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 pos);
    ~Obstacle02();

    Kcoy::SpriteRender *spriteRender;

    void update();
};

