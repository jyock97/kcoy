#include "Border.h"

Border::Border() : Kcoy::GameObject("Border")
{
    this->rigidbody = new Kcoy::RigidBody(&this->transform, true);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidbody, false, 2.5, 0.1);
}

Border::~Border()
{
    delete this->boxCollider;
    delete this->rigidbody;
}

void Border::update() { }
