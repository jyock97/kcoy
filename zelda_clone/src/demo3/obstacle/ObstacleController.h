#pragma once
#include "Kcoy.h"
#include "Border.h"
#include "TopObstacle.h"
#include "BottomObstacle.h"
#include "CheckPoint.h"

class Demo3;
class ObstacleController : public Kcoy::GameObject
{
public:
    ObstacleController(Demo3 *controllerRef);
    ~ObstacleController();

    Border *b1, *b2;
    TopObstacle *to1, *to2, *to3, *to4;
    BottomObstacle *bo1, *bo2, *bo3, *bo4;
    CheckPoint *cp1, *cp2, *cp3, *cp4;

    void update();
    void reset();

private:
    Demo3 *controllerRef;

    float getRandomYOffset(Kcoy::GameObject *obstacle);
};
