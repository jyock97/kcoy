#pragma once
#include "Kcoy.h"

class CheckPoint : public Kcoy::GameObject
{
public:
    CheckPoint();
    ~CheckPoint();

    Kcoy::RigidBody *rigidbody;
    Kcoy::BoxCollider *boxCollider;

    void update();
};

