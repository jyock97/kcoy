#include "CheckPoint.h"

CheckPoint::CheckPoint() : Kcoy::GameObject("CheckPoint")
{
    this->rigidbody = new Kcoy::RigidBody(&this->transform, true);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidbody, true, 0.1, 1);
}

CheckPoint::~CheckPoint()
{
    delete this->boxCollider;
}

void CheckPoint::update() { }
