#include "Obstacle02.h"

Obstacle02::Obstacle02(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 pos) :
    Kcoy::GameObject("Obstacle02", parentTransformRef)
{
    this->spriteRender = new Kcoy::SpriteRender(&this->transform, "assets/sprites/obstacle/obstacle02.png", 5, 32);
    this->transform.setLocalPosition(pos);
}

Obstacle02::~Obstacle02()
{
    delete this->spriteRender;
}

void Obstacle02::update() { }
