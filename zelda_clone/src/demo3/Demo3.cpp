#include "Demo3.h"

Demo3::Demo3() : Kcoy::GameObject("Demo3")
{
    this->score = new Score();
    this->background = new Background(this);
    this->obstacleController = new ObstacleController(this);
    this->player = new Player_3(this, this->score);
}

Demo3::~Demo3()
{
    delete this->player;
    delete this->obstacleController;
    delete this->background;
    delete this->score;
}

void Demo3::update() {
    if (!this->isGameStarted && Kcoy::Event::keyPressed(Kcoy::KeyCode::Space)) {
        this->startGame();
    }
    if (this->isGameStarted && Kcoy::Event::keyPressed(Kcoy::KeyCode::R)) {
        this->restartGame();
    }
}

bool Demo3::getIsGameStarted()
{
    return this->isGameStarted;
}

void Demo3::startGame()
{
    this->isGameStarted = true;
    this->score->reset();
}

void Demo3::restartGame()
{
    this->player->reset();
    this->obstacleController->reset();
    this->background->reset();

    this->isGameStarted = false;
}
