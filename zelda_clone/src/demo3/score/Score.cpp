#include "Score.h"

Score::Score() : Kcoy::GameObject("Score"), score(0) {
    this->spriteText = new Kcoy::SpriteText(&this->transform, 20, std::string("0"), 144);
    this->transform.setLocalPosition(Kcoy::Vector2(0.04f, -0.01f));
}

Score::~Score() { }

void Score::update()
{
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::W)) {
        this->spriteText->setText(std::to_string(this->score++));
    }
}

void Score::reset()
{
    this->score = 0;
    this->spriteText->setText(std::to_string(this->score));
}

void Score::scoreUp()
{
    this->score++;
    this->spriteText->setText(std::to_string(this->score));
}
