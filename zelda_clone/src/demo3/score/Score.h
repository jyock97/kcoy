#pragma once
#include "Kcoy.h"

class Score : public Kcoy::GameObject {
public:
    Score();
    ~Score();

    Kcoy::SpriteText *spriteText;

    void update();

    void reset();
    void scoreUp();

private:
    int score;
};

