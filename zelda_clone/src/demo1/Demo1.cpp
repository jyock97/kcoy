#include "Demo1.h"

Demo1::Demo1() : Kcoy::GameObject("Demo1") {
    this->player = new Player();
}

Demo1::~Demo1() { 
    delete this->player;
}

void Demo1::update() { 
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::W)) {
        Kcoy::Logger::ClientLogger()->info("Pressed W");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::S)) {
        Kcoy::Logger::ClientLogger()->info("Pressed S");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::A)) {
        Kcoy::Logger::ClientLogger()->info("Pressed A");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::D)) {
        Kcoy::Logger::ClientLogger()->info("Pressed D");
    }
}
