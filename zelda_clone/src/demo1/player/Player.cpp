#include "Player.h"

Player::Player() : Kcoy::GameObject("Player") {
    this->spriteRenderer = new Kcoy::SpriteRender(&this->transform, "assets/sprites/link/link.png", 0, 256);
}

Player::~Player() {
    delete this->spriteRenderer;
}

void Player::update() {
    float speed = 1.5f;
    Kcoy::Vector2 pos = Kcoy::Vector2(this->transform.getPosition());

    if (Kcoy::Event::keyHold(Kcoy::KeyCode::W)) {
        pos += Kcoy::Vector2(0, 1) * speed * Kcoy::Time::deltaTime();
        this->transform.setLocalPosition(pos);
    }
    if (Kcoy::Event::keyHold(Kcoy::KeyCode::S)) {
        pos += Kcoy::Vector2(0, -1) * speed * Kcoy::Time::deltaTime();
        this->transform.setLocalPosition(pos);
    }
    if (Kcoy::Event::keyHold(Kcoy::KeyCode::A)) {
        pos += Kcoy::Vector2(-1, 0) * speed * Kcoy::Time::deltaTime();
        this->transform.setLocalPosition(pos);
    }
    if (Kcoy::Event::keyHold(Kcoy::KeyCode::D)) {
        pos += Kcoy::Vector2(1, 0) * speed * Kcoy::Time::deltaTime();
        this->transform.setLocalPosition(pos);
    }
}
