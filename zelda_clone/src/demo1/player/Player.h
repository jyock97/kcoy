#pragma once
#include "Kcoy.h"

class Player : public Kcoy::GameObject {
public:
    Player();
    ~Player();

    Kcoy::SpriteRender *spriteRenderer;

    void update();
};
