#pragma once
#include "Kcoy.h"
#include "demo2/ground/Ground.h"
#include "demo2/player/Player_2.h"

class Demo2 : public Kcoy::GameObject {
public:
    Demo2();
    ~Demo2();

    Ground *ground;
    Player_2 *player;
    
    void update();
};

