#include "Player_2.h"

Player_2::Player_2() : Kcoy::GameObject("Player_2") {
    this->spriteRenderer = new Kcoy::SpriteRender(&this->transform, "assets/sprites/floor/floor.png", 0, 256, Kcoy::SpriteOrigin::Center);
    this->rigidBody = new Kcoy::RigidBody(&this->transform);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidBody, false, 1, 1);
    this->transform.setLocalPosition(Kcoy::Vector2(2.05, 0));

    this->moveSwitch = false;
    
    // This is to test how the engine prioritize the callbacks
    this->boxCollider->addCollisionStartCallback(
        [&](Kcoy::GameObject *gameObject) {
            Kcoy::Logger::ClientLogger()->info("Collision Start Detected with {}", (void*)gameObject);
            this->transform.setLocalPosition(Kcoy::Vector2(2, 0));
        });
    this->boxCollider->addCollisionStayCallback(
        [&](Kcoy::GameObject *gameObject) {
            Kcoy::Logger::ClientLogger()->info("Collision Stay Detected with {}", (void*)gameObject);
        });
    this->boxCollider->addCollisionEndCallback(
        [&](Kcoy::GameObject *gameObject) {
            Kcoy::Logger::ClientLogger()->info("Collision End Detected with {}", (void*)gameObject);
            Kcoy::Vector2 pos = Kcoy::Vector2(this->transform.getPosition());
            if (this->moveSwitch) {
                pos -= Kcoy::Vector2(0.5, 0);
            }
            else {
                pos += Kcoy::Vector2(0.5, 0);
            }
            this->transform.setLocalPosition(pos);
            this->rigidBody->setVelocity(Kcoy::Vector2(0, 0));
            this->moveSwitch = !this->moveSwitch;
        });
}

Player_2::~Player_2() {
    delete this->spriteRenderer;
    delete this->boxCollider;
    delete this->rigidBody;
}

void Player_2::update() {
    if (Kcoy::Event::keyHold(Kcoy::KeyCode::W)) {
        this->transform.setLocalPosition(Kcoy::Vector2(2.5,0));
    }
    Kcoy::Logger::ClientLogger()->info("Player Pos {} {}", this->transform.getWorldPosition().getX(), this->transform.getWorldPosition().getY());
}
