#pragma once
#include "Kcoy.h"

class Player_2 : public Kcoy::GameObject {
public:
    Player_2();
    ~Player_2();

    Kcoy::SpriteRender *spriteRenderer;
    Kcoy::RigidBody *rigidBody;
    Kcoy::BoxCollider *boxCollider;
    bool moveSwitch;

    void update();
};

