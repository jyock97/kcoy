#pragma once
#include "Kcoy.h"

class Ground : public Kcoy::GameObject {
public:
    Ground();
    ~Ground();

    Kcoy::SpriteRender *spriteRenderer;
    Kcoy::RigidBody *rigidBody;
    Kcoy::BoxCollider *boxCollider;

    void update();
};

