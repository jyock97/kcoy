#include "Ground.h"

Ground::Ground() : Kcoy::GameObject("Ground") {
    this->spriteRenderer = new Kcoy::SpriteRender(&this->transform, "assets/sprites/floor/floor.png", 0, 256);
    this->rigidBody = new Kcoy::RigidBody(&this->transform, true);
    this->boxCollider = new Kcoy::BoxCollider(&this->transform, this->rigidBody, false, 1, 1);
    this->transform.setLocalPosition(Kcoy::Vector2(2, -2));
}

Ground::~Ground() { 
    delete this->spriteRenderer;
    delete this->boxCollider;
    delete this->rigidBody;
}

void Ground::update() {
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::S)) {
        Kcoy::Vector2 pos = Kcoy::Vector2(this->transform.getPosition());
        pos += Kcoy::Vector2(0, 0.1f);
        this->transform.setLocalPosition(pos);
    }
}
