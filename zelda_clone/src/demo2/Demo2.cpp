#include "Demo2.h"

Demo2::Demo2() : Kcoy::GameObject("Demo2") {
    this->ground = new Ground();
    this->player = new Player_2();
}

Demo2::~Demo2() {
    delete this->ground;
    delete this->player;
}

void Demo2::update() {
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::W)) {
        Kcoy::Logger::ClientLogger()->info("Pressed W");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::S)) {
        Kcoy::Logger::ClientLogger()->info("Pressed S");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::A)) {
        Kcoy::Logger::ClientLogger()->info("Pressed A");
    }
    if (Kcoy::Event::keyPressed(Kcoy::KeyCode::D)) {
        Kcoy::Logger::ClientLogger()->info("Pressed D");
    }
}
