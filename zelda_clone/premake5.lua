project (projectName)
    language "C++"
    cppdialect "C++17"
    targetdir ("%{wks.location}/bin/" .. outputDir .. "/%{prj.name}")
    objdir ("%{wks.location}/bin-int/" .. outputDir .. "/%{prj.name}")

    files {
        "src/**.h",
        "src/**.cpp"
    }

    includedirs {
        "%{wks.location}/kcoy/src",
        "%{wks.location}/" .. projectName .. "/src",
        "%{wks.location}/kcoy/vendor/*/include"
    }

    libdirs {
    }

    links {
        "kcoy",
        "chipmunk"
    }

    postbuildcommands {
        "{COPY} %{wks.location}/" .. projectName .."/assets/ %{wks.location}bin/" .. outputDir .."/" .. projectName .. "/assets/"
    }

    filter "system:windows"
        systemversion "latest"
        defines {
            "KY_PLATFORM_WINDOWS",
            "SFML_STATIC"
        }

        includedirs {
            "%{wks.location}/kcoy/vendor/*/win_64/include"
        }

        libdirs {
            "%{wks.location}/kcoy/vendor/*/win_64/lib"
        }

        links {
        -- sfml links dependencies
            "freetype",
            "opengl32",
            "gdi32",
            "winmm"
        }

    filter "system:linux"
        includedirs {
            "%{wks.location}/kcoy/vendor/*/linux_64/include"
        }
        libdirs {
            "%{wks.location}/kcoy/vendor/*/linux_64/lib"
        }
        links {
            "sfml-graphics",
            "sfml-window",
            "sfml-system"
        }

        postbuildcommands {
        }

    filter "configurations:Debug"
        defines "KY_DEBUG"
        runtime "Debug"
        symbols "on"

        libdirs {
            "%{wks.location}/kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/DebugDLL"
        }

    filter "configurations:Release"
        defines "KY_RELEASE"
        runtime "Release"
        optimize "on"

        libdirs {
            "%{wks.location}/kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/ReleaseDLL"
        }

    filter "configurations:Dist"
        defines "KY_DIST"
        runtime "Release"
        optimize "on"

        libdirs {
            "%{wks.location}/kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/ReleaseDLL"
        }

    filter {"system:windows", "configurations:Debug"}
        links {
            "sfml-graphics-d",
            "sfml-window-d",
            "sfml-system-d"
        }

    filter {"system:windows", "configurations:not Debug"}
        links {
            "sfml-graphics",
            "sfml-window",
            "sfml-system"
        }

    filter "system:not windows or configurations:Debug"
        kind "ConsoleApp"
        
    filter {"system:windows", "configurations:not Debug"}
        kind "WindowedApp"
