#pragma once
#include "Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/event/Event.h"
#include "kcoy/api/gameLogic/GameObject.h"
#include <SFML/Graphics.hpp>

namespace Kcoy {
    class KCOY_API Application {
    public:
        void run();

    private:
        virtual void start() = 0;
        virtual void end() = 0;
    };

    Application *createApplication();
}
