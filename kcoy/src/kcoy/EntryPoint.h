#pragma once

#include "kcoy/api/logger/Logger.h"

extern Kcoy::Application *Kcoy::createApplication();

#if defined(KY_PLATFORM_WINDOWS) && !defined(KY_DEBUG)
INT WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCmdLine, INT nCmdShow) {
#else
int main(int argc, char **argv) {
#endif

    Kcoy::Logger::init();

    auto app = Kcoy::createApplication();
    app->run();
    delete app;

    return 0;
}
