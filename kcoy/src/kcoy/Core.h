#pragma once

#ifdef KY_PLATFORM_WINDOWS
    #ifdef KY_BUILD_DLL
        #define KCOY_API __declspec(dllexport)
    #else
        #define KCOY_API __declspec(dllimport)
    #endif

#else
    #define KCOY_API
#endif
