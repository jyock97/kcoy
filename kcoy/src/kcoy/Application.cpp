#include "Application.h"
#include "kcoy/engine/event/EventHandler.h"
#include "kcoy/engine/gameLogic/GameObjectHandler.h"
#include "kcoy/engine/time/TimeHandler.h"
#include "kcoy/engine/physics/PhysicsEngine.h"
#include "kcoy/engine/render/Render.h"

void Kcoy::Application::run() {
    Kcoy::Logger::CoreLogger()->info("Info");
    Kcoy::Logger::CoreLogger()->warn("Warn");
    Kcoy::Logger::CoreLogger()->error("Error");
    Kcoy::Logger::ClientLogger()->info("Info");
    Kcoy::Logger::ClientLogger()->warn("Warn");
    Kcoy::Logger::ClientLogger()->error("Error");

    sf::RenderWindow window(sf::VideoMode(1280, 720), "Kcoy Engine");
    //window.setFramerateLimit(5); // this is for testing

    
    /*sf::Texture texture;
    if (!texture.loadFromFile("assets/sprites/link/link.png"))
    {
        Kcoy::Logger::CoreLogger()->error("Error trying to open image from file");
    }
    sf::Sprite sprite;
    sprite.setTexture(texture);
    sf::Sprite sprite2;
    sprite2.setTexture(texture);
    float offset = 256;
    sprite2.move(offset * 1, offset * 0);*/

    /*sf::View view = window.getDefaultView();
    view.zoom(2);
    window.setView(view);*/


//#include <bitset>
//        std::bitset<4> bset;
//
//        bset.set(1);
//        Kcoy::Logger::CoreLogger()->info("{}", bset.to_string());
//        bset.reset(1);
//        Kcoy::Logger::CoreLogger()->info("{}", bset.to_string());

//#include <map>
//        std::map<GameObject*, GameObject*> m;
//        GameObject obj1 = GameObject(2);
//        GameObject obj2 = GameObject(3);
//        GameObject obj3 = GameObject(4);
//        m[&obj1] = &obj1;
//        m[&obj2] = &obj2;
//        m[&obj3] = &obj3;
//
//        for (auto it = m.begin(); it != m.end(); ++it)
//            Kcoy::Logger::CoreLogger()->info("m {} {}", it->first->myId, it->second->myId);
//
//        m.erase(&obj2);
//        Kcoy::Logger::CoreLogger()->info("----------------------------");
//        for (auto it = m.begin(); it != m.end(); ++it)
//            Kcoy::Logger::CoreLogger()->info("m {} {}", it->first->myId, it->second->myId);

    /*#include <chipmunk/chipmunk.h>

    cpVect gravity = cpv(0, 9);
    cpSpace *space = cpSpaceNew();
    cpSpaceSetGravity(space, gravity);


    cpShape *ground = cpSegmentShapeNew(cpSpaceGetStaticBody(space), cpv(-20, -2), cpv(20, -2), 0);
    cpShapeSetFriction(ground, 1);
    cpSpaceAddShape(space, ground);

    cpFloat radius = 5;
    cpFloat mass = 1;

    cpBody *ballBody = cpSpaceAddBody(space, cpBodyNew(mass, 1));
    cpBodySetPosition(ballBody, cpv(0, 0));

    cpShape *ballShape = cpSpaceAddShape(space, cpBoxShapeNew(ballBody, 1,0.5f,1));

    cpFloat timeStep = 1.0 / 60.0;
    for (cpFloat time = 0; time < 5; time += timeStep) {
        cpVect pos = cpBodyGetPosition(ballBody);
        cpVect vel = cpBodyGetVelocity(ballBody);
        printf(
            "Time is %5.2f. ballBody is at (%5.2f, %5.2f). It's velocity is (%5.2f, %5.2f)\n",
            time, pos.x, pos.y, vel.x, vel.y
        );

        cpSpaceStep(space, timeStep);
    }*/

    //return;

    /*sf::Time time;
    time.asMicroseconds();*/

    KcoyEngine::PhysicsEngine::Instance().setGravity(cpv(0, -9.8)); //consider getting this from a config file or something
    this->start();
    while (window.isOpen()) {
        KcoyEngine::EventHandler::Instance().processEvents(window);

        if (Kcoy::Event::windowEvent(Kcoy::WindowEvent::Close)) {
            window.close();
            continue;
        }

        KcoyEngine::GameObjectHandler::Instance().update();
        KcoyEngine::PhysicsEngine::Instance().step();
        KcoyEngine::Render::Instance().render(window);

        KcoyEngine::TimeHandler::Instance().setDeltaTime();
    }
    this->end();
}
