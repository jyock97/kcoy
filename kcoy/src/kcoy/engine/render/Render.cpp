#include "Render.h"

KcoyEngine::Render& KcoyEngine::Render::Instance() {
    static KcoyEngine::Render instance;

    return instance;
}

void KcoyEngine::Render::add(Kcoy::Sprite *reference)
{
    this->allSprites[reference] = reference;
}

void KcoyEngine::Render::remove(Kcoy::Sprite *reference)
{
    this->allSprites.erase(reference);
}

void KcoyEngine::Render::add(Kcoy::Gizmo *reference)
{
    this->allGizmos[reference] = reference;
}

void KcoyEngine::Render::remove(Kcoy::Gizmo *reference)
{
    this->allGizmos.erase(reference);
}

void KcoyEngine::Render::render(sf::RenderWindow &window)
{
    std::priority_queue<std::pair<unsigned int, sf::Drawable*>> spritesToRender;
    for (auto &allSpritesPair : this->allSprites) {
        Kcoy::Sprite *sprite = allSpritesPair.second;
        Kcoy::Transform *transformRef = sprite->getTransformRef();
        Kcoy::Vector2 pos = transformRef->getWorldPosition();
        pos *= KcoyEngine::Render::PixelsPerUnit;
        pos.invertY();
        sprite->getSFMLTransformable()->setPosition(pos.getX(), pos.getY());
        //Kcoy::Logger::CoreLogger()->info("Sprite Render position {} {}", transformRef->getWorldPosition().getX(), transformRef->getWorldPosition().getY());

        // TODO change the function of this queue to be desc
        spritesToRender.push(std::make_pair(-allSpritesPair.second->getPriority(), allSpritesPair.second->getSFMLDrawable()));
    }

    window.clear();

    // TODO implement better engine reference grid
    sf::RectangleShape linex(sf::Vector2f(100 * 256, 1));
    linex.setFillColor(sf::Color::Red);
    linex.setPosition(sf::Vector2f(-50 * 256, 1 * 256));
    window.draw(linex);

    sf::RectangleShape liney(sf::Vector2f(1, 100 * 256));
    liney.setFillColor(sf::Color::Red);
    liney.setPosition(sf::Vector2f(1 * 256, -50 * 256));
    window.draw(liney);

    while (!spritesToRender.empty()) {
        auto pair = spritesToRender.top();
        
        window.draw(*pair.second);
        spritesToRender.pop();
    }

#ifdef KY_DEBUG
    for (auto &gizmoPair : this->allGizmos) {
        Kcoy::Gizmo *gizmo = gizmoPair.second;
        Kcoy::Transform *transformRef = gizmo->getTransformRef();
        Kcoy::Vector2 pos = transformRef->getWorldPosition();
        pos *= KcoyEngine::Render::PixelsPerUnit;
        pos.invertY();
        gizmo->getShape()->setPosition(pos.getX(), pos.getY());
        window.draw(*gizmo->getShape());
    }
#endif // DEBUG

    /*int x = 248;
    sf::RectangleShape linex3(sf::Vector2f(x, x));
    linex3.setFillColor(sf::Color::Transparent);
    linex3.setOutlineThickness(4);
    linex3.setOutlineColor(sf::Color::Blue);
    linex3.setOrigin(x / 2, x / 2);
    linex3.setPosition(2 * 256, 2 * 256);
    window.draw(linex3);

    sf::CircleShape or(8);
    or.setFillColor(sf::Color::White);
    or.setOutlineThickness(8);
    or.setOutlineColor(sf::Color::Black);
    or.setOrigin(16 / 2, 16 / 2);
    or .setPosition(2 * 256, 2 * 256);
    window.draw(or);*/

   /* sf::RectangleShape linex2(sf::Vector2f(100 * 256, 1));
    linex2.setFillColor(sf::Color::White);
    linex2.setPosition(sf::Vector2f(-50 * 256, 2 * 256));
    window.draw(linex2);

    sf::RectangleShape liney2(sf::Vector2f(1, 100 * 256));
    liney2.setFillColor(sf::Color::White);
    liney2.setPosition(sf::Vector2f(2 * 256, -50 * 256));
    window.draw(liney2);*/

    window.display();
}
