#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/gameLogic/components/Sprite.h"
#include "kcoy/api/gizmos/Gizmo.h"
#include <queue>
#include <utility>
#include <SFML/Graphics.hpp>

namespace KcoyEngine {
    class KCOY_API Render {
    public:
        static const int PixelsPerUnit = 256;

        static Render& Instance();

        void add(Kcoy::Sprite *reference);
        void remove(Kcoy::Sprite *reference);
        void add(Kcoy::Gizmo *reference);
        void remove(Kcoy::Gizmo *reference);
        void render(sf::RenderWindow &window);

    private:
        Render() = default;
        ~Render() = default;
        Render(const Render&) = delete;
        Render& operator=(const Render&) = delete;

        std::unordered_map<Kcoy::Sprite*, Kcoy::Sprite*> allSprites;
        std::unordered_map<Kcoy::Gizmo*, Kcoy::Gizmo*> allGizmos;
    };
}