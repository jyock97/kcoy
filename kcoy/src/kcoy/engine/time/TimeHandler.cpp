#include "TimeHandler.h"

KcoyEngine::TimeHandler & KcoyEngine::TimeHandler::Instance() {
    static KcoyEngine::TimeHandler timeHandler;

    return timeHandler;
}

float KcoyEngine::TimeHandler::getDeltaTime() {
    return this->deltaTime.asSeconds();
}

void KcoyEngine::TimeHandler::setDeltaTime() {
    this->deltaTime = this->clock.restart();
}

sf::Time KcoyEngine::TimeHandler::getPhysicsTime() {
    return this->physicsClock.getElapsedTime();
}
