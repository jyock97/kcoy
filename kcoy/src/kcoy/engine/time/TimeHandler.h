#pragma once
#include "kcoy/Core.h"
#include <SFML/System.hpp>

namespace KcoyEngine {
    class KCOY_API TimeHandler {
    public:
        static TimeHandler& Instance();

        float getDeltaTime();
        void setDeltaTime();

        sf::Time getPhysicsTime();

    private:
        TimeHandler() = default;
        ~TimeHandler() = default;
        TimeHandler(const TimeHandler&) = delete;
        TimeHandler& operator=(const TimeHandler&) = delete;

        sf::Clock clock;
        sf::Time deltaTime;
        sf::Clock physicsClock;
    };
}
