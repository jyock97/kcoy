#include "PhysicsEngine.h"
#include "kcoy/engine/time/TimeHandler.h"

cpFloat KcoyEngine::PhysicsEngine::TIMESPTEP = 1.0 / 120.0;
sf::Time KcoyEngine::PhysicsEngine::TIME_TIMESPTEP = sf::seconds(KcoyEngine::PhysicsEngine::TIMESPTEP);

std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> KcoyEngine::PhysicsEngine::collisionStartShapes = std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*>();
std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> KcoyEngine::PhysicsEngine::collisionStayShapes = std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*>();
std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> KcoyEngine::PhysicsEngine::collisionEndShapes = std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*>();
std::unordered_map<cpShape*, Kcoy::BoxCollider*> KcoyEngine::PhysicsEngine::shapeColliderIndex = std::unordered_map<cpShape*, Kcoy::BoxCollider*>();

KcoyEngine::PhysicsEngine& KcoyEngine::PhysicsEngine::Instance() {
    static KcoyEngine::PhysicsEngine instance;

    return instance;
}

cpSpace *KcoyEngine::PhysicsEngine::getSpace() {
    return this->space;
}

void KcoyEngine::PhysicsEngine::setGravity(cpVect gravity) {
    cpSpaceSetGravity(this->space, gravity);
}

KcoyEngine::PhysicsEngine::PhysicsEngine() {
    this->space = cpSpaceNew();
    this->defaultCollisionHandler = cpSpaceAddDefaultCollisionHandler(this->space);
    this->defaultCollisionHandler->beginFunc = (cpCollisionBeginFunc)KcoyEngine::PhysicsEngine::beginCollisionFunction;
    this->defaultCollisionHandler->separateFunc = (cpCollisionSeparateFunc)KcoyEngine::PhysicsEngine::separateCollisionFunction;
}

KcoyEngine::PhysicsEngine::~PhysicsEngine() {
    cpSpaceFree(this->space);
}

void KcoyEngine::PhysicsEngine::step() {
    for (auto &bodyTransformPair : this->dynamicBodyTransforms) {
        cpBody *body = bodyTransformPair.first;
        Kcoy::Transform *transformRef = bodyTransformPair.second;
        cpBodySetPosition(body, cpv(transformRef->getWorldPosition().getX(), transformRef->getWorldPosition().getY()));
        cpSpaceReindexShapesForBody(this->space, body);
        //Kcoy::Logger::CoreLogger()->info("Physics position {} {}", transformRef->getWorldPosition().getX(), transformRef->getWorldPosition().getY());
    }

    sf::Time physicsTime = KcoyEngine::TimeHandler::Instance().getPhysicsTime();
    
    int debug_stepCount = 0;
    int count = 6; // TODO make this a constant or configurable value
    while ((physicsTime - this->timeAcc) > PhysicsEngine::TIME_TIMESPTEP && count--) {
        this->timeAcc += PhysicsEngine::TIME_TIMESPTEP;
        cpSpaceStep(this->space, PhysicsEngine::TIMESPTEP);

        debug_stepCount++;
    }
    Kcoy::Logger::CoreLogger()->info("PhysicsEngine - Step({})", debug_stepCount);

    // Update Bodies Position
    for (auto bodyTransformPair : this->dynamicBodyTransforms) {
        cpVect pos = cpBodyGetPosition(bodyTransformPair.first);
        cpVect vel = cpBodyGetVelocity(bodyTransformPair.first);
        cpFloat angle = cpBodyGetAngle(bodyTransformPair.first);
        bodyTransformPair.second->setWorldPosition(Kcoy::Vector2(pos.x, pos.y));
        bodyTransformPair.second->setWorldRotation(angle);

        //Kcoy::Logger::CoreLogger()->info("Body Position {} {}", pos.x, pos.y);
    }

    this->postStep();
}

void KcoyEngine::PhysicsEngine::addBody(cpBody *body, Kcoy::Transform *transform) {
    //TODO validate if the space its busy, in that case enqueue the operation for the postStep
    cpSpaceAddBody(this->space, body);
    dynamicBodyTransforms[body] = transform;
}

void KcoyEngine::PhysicsEngine::removeBody(cpBody *body) {
    cpSpaceRemoveBody(this->space, body);
    dynamicBodyTransforms.erase(body);
}

void KcoyEngine::PhysicsEngine::reindexBodyShapes(cpBody * body) {
    cpSpaceReindexShapesForBody(this->space, body);
}

void KcoyEngine::PhysicsEngine::addShape(Kcoy::BoxCollider *collider, cpShape *shape) {
    cpSpaceAddShape(this->space, shape);
    this->shapeColliderIndex[shape] = collider;
}

void KcoyEngine::PhysicsEngine::removeShape(cpShape *shape) {
    cpSpaceRemoveShape(this->space, shape);
    this->shapeColliderIndex.erase(shape);
}

void KcoyEngine::PhysicsEngine::postStep() {
    Kcoy::Logger::CoreLogger()->info("postStep");

    // Collision Callbacks
    // Start
    for (auto collisionPair : KcoyEngine::PhysicsEngine::collisionStartShapes) {
        if (KcoyEngine::PhysicsEngine::collisionStartCallbacks.find(collisionPair.first) != KcoyEngine::PhysicsEngine::collisionStartCallbacks.end()) {
            auto f = collisionStartCallbacks[collisionPair.first];
            f(collisionPair.second);
        }
    }
    KcoyEngine::PhysicsEngine::collisionStartShapes.clear();
    // Stay
    for (auto collisionPair : KcoyEngine::PhysicsEngine::collisionStayShapes) {
        if (KcoyEngine::PhysicsEngine::collisionStayCallbacks.find(collisionPair.first) != KcoyEngine::PhysicsEngine::collisionStayCallbacks.end()) {
            auto f = collisionStayCallbacks[collisionPair.first];
            f(collisionPair.second);
        }
    }
    // End
    for (auto collisionPair : KcoyEngine::PhysicsEngine::collisionEndShapes) {
        if (KcoyEngine::PhysicsEngine::collisionEndCallbacks.find(collisionPair.first) != KcoyEngine::PhysicsEngine::collisionEndCallbacks.end()) {
            auto f = collisionEndCallbacks[collisionPair.first];
            f(collisionPair.second);
        }
        KcoyEngine::PhysicsEngine::collisionStayShapes.erase(collisionPair.first);
    }
    KcoyEngine::PhysicsEngine::collisionEndShapes.clear();
}


int KcoyEngine::PhysicsEngine::beginCollisionFunction(cpArbiter *arb, cpSpace *space, cpDataPointer data) {
    cpShape *a, *b;
    cpArbiterGetShapes(arb, &a, &b);
    Kcoy::BoxCollider *colliderA = KcoyEngine::PhysicsEngine::shapeColliderIndex[a];
    Kcoy::BoxCollider *colliderB = KcoyEngine::PhysicsEngine::shapeColliderIndex[b];

    KcoyEngine::PhysicsEngine::collisionStartShapes[colliderA] = colliderB->gameObjectRef;
    KcoyEngine::PhysicsEngine::collisionStartShapes[colliderB] = colliderA->gameObjectRef;
    KcoyEngine::PhysicsEngine::collisionStayShapes[colliderA] = colliderB->gameObjectRef;
    KcoyEngine::PhysicsEngine::collisionStayShapes[colliderB] = colliderA->gameObjectRef;
    return 1;
}

void KcoyEngine::PhysicsEngine::separateCollisionFunction(cpArbiter *arb, cpSpace *space, cpDataPointer data) {
    cpShape *a, *b;
    cpArbiterGetShapes(arb, &a, &b);
    Kcoy::BoxCollider *colliderA = KcoyEngine::PhysicsEngine::shapeColliderIndex[a];
    Kcoy::BoxCollider *colliderB = KcoyEngine::PhysicsEngine::shapeColliderIndex[b];

    KcoyEngine::PhysicsEngine::collisionEndShapes[colliderA] = colliderB->gameObjectRef;
    KcoyEngine::PhysicsEngine::collisionEndShapes[colliderB] = colliderA->gameObjectRef;
}


void KcoyEngine::PhysicsEngine::addCollisionStartCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f) {
    this->collisionStartCallbacks[collider] = f;
}

void KcoyEngine::PhysicsEngine::removeCollisionStartCallback(Kcoy::BoxCollider *collider) {
    this->collisionStartCallbacks.erase(collider);
}

void KcoyEngine::PhysicsEngine::addCollisionStayCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f) {
    this->collisionStayCallbacks[collider] = f;
}

void KcoyEngine::PhysicsEngine::removeCollisionStayCallback(Kcoy::BoxCollider *collider) {
    this->collisionStayCallbacks.erase(collider);
}

void KcoyEngine::PhysicsEngine::addCollisionEndCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f) {
    this->collisionEndCallbacks[collider] = f;
}

void KcoyEngine::PhysicsEngine::removeCollisionEndCallback(Kcoy::BoxCollider *collider) {
    this->collisionEndCallbacks.erase(collider);
}
