#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/gameLogic/GameObject.h"
#include "kcoy/api/gameLogic/components/Transform.h"
#include "kcoy/api/gameLogic/components/BoxCollider.h"
#include <unordered_map>
#include <chipmunk/chipmunk.h>
#include <SFML/System.hpp>


// TODO investigate why an object with gravity, always increase the velocity(no air friction) its this intended?, can cause collision some problems?
namespace KcoyEngine {
    class KCOY_API PhysicsEngine {
    public:
        static cpFloat TIMESPTEP;
        static sf::Time TIME_TIMESPTEP;
        static PhysicsEngine& Instance();
        static int beginCollisionFunction(cpArbiter *arb, cpSpace *space, cpDataPointer data);
        static void separateCollisionFunction(cpArbiter *arb, cpSpace *space, cpDataPointer data);
        
        void addCollisionStartCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f);
        void removeCollisionStartCallback(Kcoy::BoxCollider *collider);
        void addCollisionStayCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f);
        void removeCollisionStayCallback(Kcoy::BoxCollider *collider);
        void addCollisionEndCallback(Kcoy::BoxCollider *collider, std::function<void(Kcoy::GameObject*)> f);
        void removeCollisionEndCallback(Kcoy::BoxCollider *collider);

        cpSpace *getSpace();
        void setGravity(cpVect gravity);

        void step();
        void addBody(cpBody *body, Kcoy::Transform *transform);
        void removeBody(cpBody *body);
        void reindexBodyShapes(cpBody *body);

        void addShape(Kcoy::BoxCollider *collider, cpShape *shape);
        void removeShape(cpShape *shape);

    private:
        PhysicsEngine();
        ~PhysicsEngine();
        PhysicsEngine(const PhysicsEngine&) = delete;
        PhysicsEngine& operator=(const PhysicsEngine&) = delete;

        static std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> collisionStartShapes;
        static std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> collisionStayShapes;
        static std::unordered_map<Kcoy::BoxCollider*, Kcoy::GameObject*> collisionEndShapes;
        static std::unordered_map<cpShape*, Kcoy::BoxCollider*> shapeColliderIndex;
        std::unordered_map<Kcoy::BoxCollider*, std::function<void(Kcoy::GameObject*)>> collisionStartCallbacks;
        std::unordered_map<Kcoy::BoxCollider*, std::function<void(Kcoy::GameObject*)>> collisionStayCallbacks;
        std::unordered_map<Kcoy::BoxCollider*, std::function<void(Kcoy::GameObject*)>> collisionEndCallbacks;

        cpSpace *space;
        cpCollisionHandler *defaultCollisionHandler;
        sf::Time timeAcc;

        std::unordered_map<cpBody*, Kcoy::Transform*> dynamicBodyTransforms;

        void postStep();
    };
}
