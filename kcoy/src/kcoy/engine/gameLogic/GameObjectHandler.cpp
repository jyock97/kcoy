#include "GameObjectHandler.h"

KcoyEngine::GameObjectHandler & KcoyEngine::GameObjectHandler::Instance()
{
    static KcoyEngine::GameObjectHandler instance;

    return instance;   
}

void KcoyEngine::GameObjectHandler::update()
{
    for (auto it = KcoyEngine::GameObjectHandler::gameObjets.begin(); it != KcoyEngine::GameObjectHandler::gameObjets.end(); it++) {
        it->second->update();
    }
}

void KcoyEngine::GameObjectHandler::add(Kcoy::GameObject *reference)
{
    KcoyEngine::GameObjectHandler::gameObjets[reference] = reference;
}

void KcoyEngine::GameObjectHandler::remove(Kcoy::GameObject *reference)
{
    KcoyEngine::GameObjectHandler::gameObjets.erase(reference);
}
