#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/gameLogic/GameObject.h"
#include <unordered_map>

namespace KcoyEngine {
    class KCOY_API GameObjectHandler
    {
    public:
        static GameObjectHandler& Instance();

        void update();
        void add(Kcoy::GameObject *reference); //TODO consider creating an array to be cache friendly
        void remove(Kcoy::GameObject *reference);

    private:
        GameObjectHandler() = default;
        ~GameObjectHandler() = default;
        GameObjectHandler(const GameObjectHandler&) = delete;
        GameObjectHandler& operator=(const GameObjectHandler&) = delete;

        std::unordered_map<Kcoy::GameObject*, Kcoy::GameObject*> gameObjets; //TODO maybe in the future we need to sort this
    };
}
