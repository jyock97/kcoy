#include "EventHandler.h"

KcoyEngine::EventHandler& KcoyEngine::EventHandler::Instance() {
    static KcoyEngine::EventHandler instance;

    return instance;
}

void KcoyEngine::EventHandler::processEvents(sf::RenderWindow &window) {
    // reset bitsets
    windowEventsBitset.reset();
    keyPressedBitset.reset();
    keyReleasedBitset.reset();

    sf::Event event;
    while (window.pollEvent(event)) {
        switch (event.type) {
        /***** Window Events *****/
        case sf::Event::Closed:
            setWindowEvent(Kcoy::WindowEvent::Close);
            break;
        case sf::Event::Resized:
            setWindowEvent(Kcoy::WindowEvent::Resized);
            // TODO save resized values
            break;
        case sf::Event::LostFocus:
            setWindowEvent(Kcoy::WindowEvent::LostFocus);
            break;
        case sf::Event::GainedFocus:
            setWindowEvent(Kcoy::WindowEvent::GainedFocus);
            break;

        /***** Key Events *****/
        case sf::Event::KeyPressed:
            setKeyPressed(convertKeyCode(event.key.code));
            break;
        case sf::Event::KeyReleased:
            setKeyReleased(convertKeyCode(event.key.code));
            break;
        }
    }
}

void KcoyEngine::EventHandler::setWindowEvent(Kcoy::WindowEvent windowEvent) {
    this->windowEventsBitset.set((int) windowEvent);
}
bool KcoyEngine::EventHandler::isWindowEventSet(Kcoy::WindowEvent windowEvent) {
    return this->windowEventsBitset.test((int) windowEvent);
}

void KcoyEngine::EventHandler::setKeyPressed(Kcoy::KeyCode keyCode) {
    if (keyHoldBitset.test((int) keyCode)) return;

    keyPressedBitset.set((int) keyCode);
    keyHoldBitset.set((int) keyCode);

}
bool KcoyEngine::EventHandler::isKeyPressedSet(Kcoy::KeyCode keyCode) {
    return keyPressedBitset.test((int) keyCode);
}

bool KcoyEngine::EventHandler::isKeyHoldSet(Kcoy::KeyCode keyCode) {
    return keyHoldBitset.test((int) keyCode);
}

void KcoyEngine::EventHandler::setKeyReleased(Kcoy::KeyCode keyCode) {
    if (keyHoldBitset.test((int) keyCode))
        keyHoldBitset.reset((int) keyCode);

    keyReleasedBitset.set((int) keyCode);
}
bool KcoyEngine::EventHandler::isKeyReleasedSet(Kcoy::KeyCode keyCode) {
    return keyReleasedBitset.test((int) keyCode);
}

// TODO re-think how to solve this (maybe not the best approach)
Kcoy::KeyCode KcoyEngine::EventHandler::convertKeyCode(sf::Keyboard::Key keyCode) {
    switch (keyCode) {
    case sf::Keyboard::A:
        return Kcoy::KeyCode::A;
    case sf::Keyboard::B:
        return Kcoy::KeyCode::B;
    case sf::Keyboard::C:
        return Kcoy::KeyCode::C;
    case sf::Keyboard::D:
        return Kcoy::KeyCode::D;
    case sf::Keyboard::E:
        return Kcoy::KeyCode::E;
    case sf::Keyboard::F:
        return Kcoy::KeyCode::F;
    case sf::Keyboard::G:
        return Kcoy::KeyCode::G;
    case sf::Keyboard::H:
        return Kcoy::KeyCode::H;
    case sf::Keyboard::I:
        return Kcoy::KeyCode::I;
    case sf::Keyboard::J:
        return Kcoy::KeyCode::J;
    case sf::Keyboard::K:
        return Kcoy::KeyCode::K;
    case sf::Keyboard::L:
        return Kcoy::KeyCode::L;
    case sf::Keyboard::M:
        return Kcoy::KeyCode::M;
    case sf::Keyboard::N:
        return Kcoy::KeyCode::N;
    case sf::Keyboard::O:
        return Kcoy::KeyCode::O;
    case sf::Keyboard::P:
        return Kcoy::KeyCode::P;
    case sf::Keyboard::Q:
        return Kcoy::KeyCode::Q;
    case sf::Keyboard::R:
        return Kcoy::KeyCode::R;
    case sf::Keyboard::S:
        return Kcoy::KeyCode::S;
    case sf::Keyboard::T:
        return Kcoy::KeyCode::T;
    case sf::Keyboard::U:
        return Kcoy::KeyCode::U;
    case sf::Keyboard::V:
        return Kcoy::KeyCode::V;
    case sf::Keyboard::W:
        return Kcoy::KeyCode::W;
    case sf::Keyboard::X:
        return Kcoy::KeyCode::X;
    case sf::Keyboard::Y:
        return Kcoy::KeyCode::Y;
    case sf::Keyboard::Z:
        return Kcoy::KeyCode::Z;
    case sf::Keyboard::Num0:
        return Kcoy::KeyCode::Num0;
    case sf::Keyboard::Num1:
        return Kcoy::KeyCode::Num1;
    case sf::Keyboard::Num2:
        return Kcoy::KeyCode::Num2;
    case sf::Keyboard::Num3:
        return Kcoy::KeyCode::Num3;
    case sf::Keyboard::Num4:
        return Kcoy::KeyCode::Num4;
    case sf::Keyboard::Num5:
        return Kcoy::KeyCode::Num5;
    case sf::Keyboard::Num6:
        return Kcoy::KeyCode::Num6;
    case sf::Keyboard::Num7:
        return Kcoy::KeyCode::Num7;
    case sf::Keyboard::Num8:
        return Kcoy::KeyCode::Num8;
    case sf::Keyboard::Num9:
        return Kcoy::KeyCode::Num9;
    case sf::Keyboard::Escape:
        return Kcoy::KeyCode::Escape;
    case sf::Keyboard::LControl:
        return Kcoy::KeyCode::LControl;
    case sf::Keyboard::LShift:
        return Kcoy::KeyCode::LShift;
    case sf::Keyboard::LAlt:
        return Kcoy::KeyCode::LAlt;
    case sf::Keyboard::LSystem:
        return Kcoy::KeyCode::LSystem;
    case sf::Keyboard::RControl:
        return Kcoy::KeyCode::RControl;
    case sf::Keyboard::RShift:
        return Kcoy::KeyCode::RShift;
    case sf::Keyboard::RAlt:
        return Kcoy::KeyCode::RAlt;
    case sf::Keyboard::RSystem:
        return Kcoy::KeyCode::RSystem;
    case sf::Keyboard::Menu:
        return Kcoy::KeyCode::Menu;
    case sf::Keyboard::LBracket:
        return Kcoy::KeyCode::LBracket;
    case sf::Keyboard::RBracket:
        return Kcoy::KeyCode::RBracket;
    case sf::Keyboard::Semicolon:
        return Kcoy::KeyCode::Semicolon;
    case sf::Keyboard::Comma:
        return Kcoy::KeyCode::Comma;
    case sf::Keyboard::Period:
        return Kcoy::KeyCode::Period;
    case sf::Keyboard::Quote:
        return Kcoy::KeyCode::Quote;
    case sf::Keyboard::Slash:
        return Kcoy::KeyCode::Slash;
    case sf::Keyboard::Backslash:
        return Kcoy::KeyCode::Backslash;
    case sf::Keyboard::Tilde:
        return Kcoy::KeyCode::Tilde;
    case sf::Keyboard::Equal:
        return Kcoy::KeyCode::Equal;
    case sf::Keyboard::Hyphen:
        return Kcoy::KeyCode::Hyphen;
    case sf::Keyboard::Space:
        return Kcoy::KeyCode::Space;
    case sf::Keyboard::Enter:
        return Kcoy::KeyCode::Enter;
    case sf::Keyboard::Backspace:
        return Kcoy::KeyCode::Backspace;
    case sf::Keyboard::Tab:
        return Kcoy::KeyCode::Tab;
    case sf::Keyboard::PageUp:
        return Kcoy::KeyCode::PageUp;
    case sf::Keyboard::PageDown:
        return Kcoy::KeyCode::PageDown;
    case sf::Keyboard::End:
        return Kcoy::KeyCode::End;
    case sf::Keyboard::Home:
        return Kcoy::KeyCode::Home;
    case sf::Keyboard::Insert:
        return Kcoy::KeyCode::Insert;
    case sf::Keyboard::Delete:
        return Kcoy::KeyCode::Delete;
    case sf::Keyboard::Add:
        return Kcoy::KeyCode::Add;
    case sf::Keyboard::Subtract:
        return Kcoy::KeyCode::Subtract;
    case sf::Keyboard::Multiply:
        return Kcoy::KeyCode::Multiply;
    case sf::Keyboard::Divide:
        return Kcoy::KeyCode::Divide;
    case sf::Keyboard::Left:
        return Kcoy::KeyCode::Left;
    case sf::Keyboard::Right:
        return Kcoy::KeyCode::Right;
    case sf::Keyboard::Up:
        return Kcoy::KeyCode::Up;
    case sf::Keyboard::Down:
        return Kcoy::KeyCode::Down;
    case sf::Keyboard::Numpad0:
        return Kcoy::KeyCode::Numpad0;
    case sf::Keyboard::Numpad1:
        return Kcoy::KeyCode::Numpad1;
    case sf::Keyboard::Numpad2:
        return Kcoy::KeyCode::Numpad2;
    case sf::Keyboard::Numpad3:
        return Kcoy::KeyCode::Numpad3;
    case sf::Keyboard::Numpad4:
        return Kcoy::KeyCode::Numpad4;
    case sf::Keyboard::Numpad5:
        return Kcoy::KeyCode::Numpad5;
    case sf::Keyboard::Numpad6:
        return Kcoy::KeyCode::Numpad6;
    case sf::Keyboard::Numpad7:
        return Kcoy::KeyCode::Numpad7;
    case sf::Keyboard::Numpad8:
        return Kcoy::KeyCode::Numpad8;
    case sf::Keyboard::Numpad9:
        return Kcoy::KeyCode::Numpad9;
    case sf::Keyboard::F1:
        return Kcoy::KeyCode::F1;
    case sf::Keyboard::F2:
        return Kcoy::KeyCode::F2;
    case sf::Keyboard::F3:
        return Kcoy::KeyCode::F3;
    case sf::Keyboard::F4:
        return Kcoy::KeyCode::F4;
    case sf::Keyboard::F5:
        return Kcoy::KeyCode::F5;
    case sf::Keyboard::F6:
        return Kcoy::KeyCode::F6;
    case sf::Keyboard::F7:
        return Kcoy::KeyCode::F7;
    case sf::Keyboard::F8:
        return Kcoy::KeyCode::F8;
    case sf::Keyboard::F9:
        return Kcoy::KeyCode::F9;
    case sf::Keyboard::F10:
        return Kcoy::KeyCode::F10;
    case sf::Keyboard::F11:
        return Kcoy::KeyCode::F11;
    case sf::Keyboard::F12:
        return Kcoy::KeyCode::F12;
    case sf::Keyboard::F13:
        return Kcoy::KeyCode::F12;
    case sf::Keyboard::F14:
        return Kcoy::KeyCode::F14;
    case sf::Keyboard::F15:
        return Kcoy::KeyCode::F15;
    case sf::Keyboard::Pause:
        return Kcoy::KeyCode::Pause;
    case sf::Keyboard::Unknown:
    default:
        return Kcoy::KeyCode::Unknown;
    }
}
