#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/event/EventEnums.h"
#include <bitset>
#include <SFML/Graphics.hpp>

namespace KcoyEngine {
    class KCOY_API EventHandler {
    public:
        static EventHandler& Instance();

        /* Function that go through all the events and prepare the engine client to start using window events with Event.h */
        void processEvents(sf::RenderWindow &window);

        void setWindowEvent(Kcoy::WindowEvent windowEvent);
        bool isWindowEventSet(Kcoy::WindowEvent windowEvent);

        void setKeyPressed(Kcoy::KeyCode keyCode);
        bool isKeyPressedSet(Kcoy::KeyCode keyCode);

        bool isKeyHoldSet(Kcoy::KeyCode keyCode);

        void setKeyReleased(Kcoy::KeyCode keyCode);
        bool isKeyReleasedSet(Kcoy::KeyCode keyCode);

    private:
        EventHandler() = default;
        ~EventHandler() = default;
        EventHandler(const EventHandler&) = delete;
        EventHandler& operator=(const EventHandler&) = delete;

        std::bitset<(int) Kcoy::WindowEvent::WindowEventCount> windowEventsBitset;
        std::bitset<(int) Kcoy::KeyCode::KeyCodeCount> keyPressedBitset;
        std::bitset<(int) Kcoy::KeyCode::KeyCodeCount> keyHoldBitset;
        std::bitset<(int) Kcoy::KeyCode::KeyCodeCount> keyReleasedBitset;

        static Kcoy::KeyCode convertKeyCode(sf::Keyboard::Key keyCode);
    };
}
