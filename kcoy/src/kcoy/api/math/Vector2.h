#pragma once
#include "kcoy/Core.h"

namespace Kcoy {
    class KCOY_API Vector2 { //TODO use sfml::Vector classes or any other Vector class
    public:
        Vector2();
        Vector2(float x, float y);
        Vector2(const Vector2 &other);
        ~Vector2();

        float getX();
        void setX(float x);
        float getY();
        void setY(float y);
        void invertY();
        void setXY(float x, float y);
        void setXY(Vector2 &other);

        Vector2 operator+(const float n);
        Vector2 operator+(const Vector2 &other);
        Vector2 operator+(const Vector2 *other);
        Vector2 operator+=(const float n);
        Vector2 operator+=(const Vector2 &other);
        Vector2 operator+=(const Vector2 *other);

        Vector2 operator-(const float n);
        Vector2 operator-(const Vector2 &other);
        Vector2 operator-(const Vector2 *other);
        Vector2 operator-=(const float n);
        Vector2 operator-=(const Vector2 &other);
        Vector2 operator-=(const Vector2 *other);

        Vector2 operator*(const float n);
        Vector2 operator*(const Vector2 &other);
        Vector2 operator*(const Vector2 *other);
        Vector2 operator*=(const float n);
        Vector2 operator*=(const Vector2 &other);
        Vector2 operator*=(const Vector2 *other);

        Vector2 operator/(const float n);
        Vector2 operator/(const Vector2 &other);
        Vector2 operator/(const Vector2 *other);
        Vector2 operator/=(const float n);
        Vector2 operator/=(const Vector2 &other);
        Vector2 operator/=(const Vector2 *other);

        float dot(const Vector2 &other);
        float dot(const Vector2 *other);

    private:
        float *axis = new float[2];
    };
}
