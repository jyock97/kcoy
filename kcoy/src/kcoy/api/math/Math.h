#pragma once
#include "kcoy/Core.h"

namespace Kcoy {
    class KCOY_API Math
    {
    public:
        static constexpr float Deg2Rad = 0.01745329;
        static constexpr float Rad2Deg = 57.29578;

        static float sinR(float radians);
        static float cosR(float radians);
        static float tanR(float radians);
    };
}
