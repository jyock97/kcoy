#include "Math.h"
#include <math.h>

float Kcoy::Math::sinR(float radians)
{
    return sin(radians);
}

float Kcoy::Math::cosR(float radians)
{
    return cos(radians);
}

float Kcoy::Math::tanR(float radians)
{
    return tan(radians);
}
