#include "Vector2.h"

Kcoy::Vector2::Vector2()
{
    this->axis[0] = this->axis[1] = 0;
}
Kcoy::Vector2::Vector2(float x, float y)
{
    this->axis[0] = x;
    this->axis[1] = y;
}
Kcoy::Vector2::Vector2(const Vector2 &other)
{
    this->axis[0] = other.axis[0];
    this->axis[1] = other.axis[1];
}

Kcoy::Vector2::~Vector2()
{
    delete axis;
}

float Kcoy::Vector2::getX()
{
    return this->axis[0];
}
void Kcoy::Vector2::setX(float x)
{
    this->axis[0] = x;
}
float Kcoy::Vector2::getY()
{
    return this->axis[1];
}
void Kcoy::Vector2::setY(float y)
{
    this->axis[1] = y;
}

void Kcoy::Vector2::invertY()
{
    this->axis[1] *= -1;
}

void Kcoy::Vector2::setXY(float x, float y)
{
    this->axis[0] = x;
    this->axis[1] = y;
}

void Kcoy::Vector2::setXY(Vector2 & other)
{
    this->setXY(other.getX(), other.getY());
}


Kcoy::Vector2 Kcoy::Vector2::operator+(const float n)
{
    return Kcoy::Vector2(this->axis[0] + n, this->axis[1] + n);
}
Kcoy::Vector2 Kcoy::Vector2::operator+(const Vector2 &other)
{
    return Kcoy::Vector2(this->axis[0] + other.axis[0], this->axis[1] + other.axis[1]);
}
Kcoy::Vector2 Kcoy::Vector2::operator+(const Vector2 *other)
{
    return *this + *other;
}
Kcoy::Vector2 Kcoy::Vector2::operator+=(const float n) {
    this->axis[0] += n;
    this->axis[1] += n;

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator+=(const Vector2 &other)
{
    this->axis[0] += other.axis[0];
    this->axis[1] += other.axis[1];

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator+=(const Vector2 *other) {
    return *this += *other;
}


Kcoy::Vector2 Kcoy::Vector2::operator-(const float n)
{
    return Kcoy::Vector2(this->axis[0] - n, this->axis[1] - n);
}
Kcoy::Vector2 Kcoy::Vector2::operator-(const Vector2 & other)
{
    return Kcoy::Vector2(this->axis[0] - other.axis[0], this->axis[1] - other.axis[1]);
}
Kcoy::Vector2 Kcoy::Vector2::operator-(const Vector2 *other)
{
    return *this - *other;
}
Kcoy::Vector2 Kcoy::Vector2::operator-=(const float n) {
    this->axis[0] -= n;
    this->axis[1] -= n;

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator-=(const Vector2 &other) {
    this->axis[0] -= other.axis[0];
    this->axis[1] -= other.axis[1];

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator-=(const Vector2 *other) {
    return *this -= *other;
}


Kcoy::Vector2 Kcoy::Vector2::operator*(const float n)
{
    return Kcoy::Vector2(this->axis[0] * n, this->axis[1] * n);
}
Kcoy::Vector2 Kcoy::Vector2::operator*(const Vector2 &other)
{
    return Vector2(this->axis[0] * other.axis[0], this->axis[1] * other.axis[1]);
}
Kcoy::Vector2 Kcoy::Vector2::operator*(const Vector2 *other)
{
    return *this * *other;
}
Kcoy::Vector2 Kcoy::Vector2::operator*=(const float n) {
    this->axis[0] *= n;
    this->axis[1] *= n;

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator*=(const Vector2 &other)
{
    this->axis[0] *= other.axis[0];
    this->axis[1] *= other.axis[1];

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator*=(const Vector2 * other)
{
    return *this *= *other;

}


Kcoy::Vector2 Kcoy::Vector2::operator/(const float n)
{
    return Vector2(this->axis[0] / n, this->axis[1] / n);
}
Kcoy::Vector2 Kcoy::Vector2::operator/(const Vector2 &other)
{
    return Vector2(this->axis[0] / other.axis[0], this->axis[1] / other.axis[1]);
}
Kcoy::Vector2 Kcoy::Vector2::operator/(const Vector2 *other)
{
    return *this / *other;
}
Kcoy::Vector2 Kcoy::Vector2::operator/=(const float n)
{
    this->axis[0] /= n;
    this->axis[1] /= n;

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator/=(const Vector2 &other)
{
    this->axis[0] /= other.axis[0];
    this->axis[1] /= other.axis[1];

    return *this;
}
Kcoy::Vector2 Kcoy::Vector2::operator/=(const Vector2 *other)
{
    return *this /= *other;
}

float Kcoy::Vector2::dot(const Vector2 &other)
{
    return (float)((double)this->axis[0] * (double)other.axis[0] + (double)this->axis[1] * (double) other.axis[1]);
}

float Kcoy::Vector2::dot(const Vector2 *other)
{
    return this->dot(*other);
}


// This can cause memory leaks... refactor or just use Vector(x,x) instead
//Kcoy::Vector2 *Kcoy::Vector2::zero() { return new Vector2(0,0); } // TODO optimize this to return the same pointer or make it const static
//Kcoy::Vector2 *Kcoy::Vector2::one() { return new Vector2(1,1); }
//Kcoy::Vector2 *Kcoy::Vector2::up() { return new Vector2(0,1); }
//Kcoy::Vector2 *Kcoy::Vector2::down() { return new Vector2(0,-1); }
//Kcoy::Vector2 *Kcoy::Vector2::left() { return new Vector2(-1,0); }
//Kcoy::Vector2 *Kcoy::Vector2::right() { return new Vector2(1,0); }
