#include "SpriteText.h"
#include "kcoy/engine/render/Render.h"

Kcoy::SpriteText::SpriteText(Kcoy::Transform * transformRef, int priority, std::string & text, int fontSize, Kcoy::SpriteOrigin origin) :
    Kcoy::Sprite(transformRef, priority, pixelsPerUnit), textStr(text) {

    if (!font.loadFromFile("assets/fonts/roboto_bold.ttf")) { // TODO fix this to load only once
        Kcoy::Logger::CoreLogger()->error("Error trying to texture from file:{}", "assets/fonts/roboto_bold.ttf");
    }
    this->text = sf::Text(sf::String(this->textStr.c_str()), font);
    this->text.setFillColor(sf::Color::White); // TODO create a color class
    this->text.setCharacterSize(fontSize);

    KcoyEngine::Render::Instance().add(this);
}

Kcoy::SpriteText::SpriteText(Kcoy::Transform * transformRef, int priority, std::string &text, int fontSize) :
    Kcoy::SpriteText(transformRef, priority, text, fontSize, Kcoy::SpriteOrigin::UpLeft) { }

Kcoy::SpriteText::~SpriteText()
{
    KcoyEngine::Render::Instance().remove(this);
}

void Kcoy::SpriteText::setText(std::string & text)
{
    this->text.setString(text.c_str());
}

sf::Drawable *Kcoy::SpriteText::getSFMLDrawable()
{
    return (sf::Drawable*) &this->text;
}

sf::Transformable *Kcoy::SpriteText::getSFMLTransformable()
{
    return (sf::Transformable*) &this->text;
}