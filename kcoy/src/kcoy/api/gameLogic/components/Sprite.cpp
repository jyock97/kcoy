#include "Sprite.h"
#include "kcoy/engine/render/Render.h"

Kcoy::Sprite::Sprite(Kcoy::Transform *transformRef, const unsigned int priority, const unsigned int pixelsPerUnit) :
    transformRef(transformRef), priority(priority)
{ }

Kcoy::Sprite::~Sprite()
{ }

void Kcoy::Sprite::enable() {
    KcoyEngine::Render::Instance().add(this);
}
void Kcoy::Sprite::disable() {
    KcoyEngine::Render::Instance().remove(this);
}

Kcoy::Transform *Kcoy::Sprite::getTransformRef() {
    return this->transformRef;
}

unsigned int Kcoy::Sprite::getPriority() {
    return this->priority;
}

sf::Vector2f Kcoy::Sprite::calculateOrigin(const sf::Vector2u &size, SpriteOrigin origin) {
    sf::Vector2f spriteOrigin = sf::Vector2f(size.x, size.y);
    switch (origin)
    {
    case Kcoy::SpriteOrigin::Center:
        spriteOrigin.x /= 2;
        spriteOrigin.y /= 2;
        break;
    case Kcoy::SpriteOrigin::CenterUp:
        spriteOrigin.x /= 2;
        spriteOrigin.y = 0;
        break;
    case Kcoy::SpriteOrigin::CenterRight:
        spriteOrigin.y /= 2;
        break;
    case Kcoy::SpriteOrigin::CenterDown:
        spriteOrigin.x /= 2;
        break;
    case Kcoy::SpriteOrigin::CenterLeft:
        spriteOrigin.x = 0;
        spriteOrigin.y /= 2;
        break;
    case Kcoy::SpriteOrigin::UpLeft:
        spriteOrigin.x = 0;
        spriteOrigin.y = 0;
        break;
    case Kcoy::SpriteOrigin::UpRight:
        spriteOrigin.y = 0;
        break;
    case Kcoy::SpriteOrigin::DownLeft:
        spriteOrigin.x = 0;
        break;
    default:
        break;
    }

    return spriteOrigin;
}
