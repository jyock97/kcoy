#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/gameLogic/components/Sprite.h"
#include "kcoy/api/gizmos/CircleGizmo.h"
#include "kcoy/api/math/Vector2.h"
#include <string>

namespace Kcoy {
    class KCOY_API SpriteRender : public Kcoy::Sprite {
    public:
        SpriteRender(Kcoy::Transform *transformRef, const std::string &filePath, unsigned int priority, unsigned int pixelsPerUnit);
        SpriteRender(Kcoy::Transform *transformRef, const std::string &filePath, unsigned int priority, unsigned int pixelsPerUnit, SpriteOrigin origin);
        ~SpriteRender();

        sf::Drawable *getSFMLDrawable();
        sf::Transformable *getSFMLTransformable();

    private:
        std::string filePath;
        Kcoy::Gizmo *gizmo;
        
        sf::Texture texture;
        sf::Sprite sprite;
    };
}
