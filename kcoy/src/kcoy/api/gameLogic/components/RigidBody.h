#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/math/Vector2.h"
#include "kcoy/api/observer/ObserverFunction.h"
#include "kcoy/api/gameLogic/components/Transform.h"
#include <chipmunk/chipmunk.h>

namespace Kcoy {
    class KCOY_API RigidBody {
    public:
        cpBody *body;

        RigidBody(Kcoy::Transform *transformRef, bool isStatic = false);
        RigidBody(Kcoy::Transform *transformRef, cpFloat mass, bool isStatic = false);// maybe its not needed
        RigidBody(Kcoy::Transform *transformRef, cpFloat mass, cpVect pos, bool isStatic = false);// maybe its not needed
        ~RigidBody();

        Kcoy::Vector2 &getVelocity();
        void setVelocity(Kcoy::Vector2 velocity);

    private:
        Kcoy::Transform *transformRef;
        Kcoy::Vector2 velocity;

        void createBody(cpFloat mass, cpVect pos, bool isStatic);
    };
}
