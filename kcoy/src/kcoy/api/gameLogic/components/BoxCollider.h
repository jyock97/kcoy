#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/gameLogic/GameObject.h"
#include "kcoy/api/gameLogic/components/Transform.h"
#include "kcoy/api/gameLogic/components/RigidBody.h"
#include "kcoy/api/observer/ObserverFunction.h"
#include "kcoy/api/gizmos/BoxGizmo.h"
#include <functional>
#include <chipmunk/chipmunk.h>

namespace Kcoy {
    class KCOY_API BoxCollider {
    public:
        BoxCollider(Kcoy::Transform *transformRef, Kcoy::RigidBody *rigidBodyRef, bool isSensor, float width, float height, float radius = 0.0f); // TODO invest time figuring out how the radius works
        ~BoxCollider();

        Kcoy::GameObject *gameObjectRef;

        void addCollisionStartCallback(std::function<void(Kcoy::GameObject*)> f);
        void addCollisionStayCallback(std::function<void(Kcoy::GameObject*)> f);
        void addCollisionEndCallback(std::function<void(Kcoy::GameObject*)> f);

        //void updateShape(cpVect pos, cpVect rot);

    private:
        cpShape *shape;

        Kcoy::Transform *transformRef;
        Kcoy::Gizmo *gizmo;
    };
}
