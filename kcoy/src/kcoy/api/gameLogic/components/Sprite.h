#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/gameLogic/components/Transform.h"
#include "SFML/Graphics.hpp"

namespace Kcoy {
    enum class SpriteOrigin {
        Center,
        CenterUp,
        CenterRight,
        CenterDown,
        CenterLeft,
        UpLeft,
        UpRight,
        DownLeft,
        DownRight,

        SpriteOriginCount
    };

    class KCOY_API Sprite
    {
    public:
        Sprite(Kcoy::Transform *transformRef, const unsigned int priority, const unsigned int pixelsPerUnit);
        ~Sprite();

        void enable();
        void disable();

        Kcoy::Transform *getTransformRef();
        unsigned int getPriority();

        virtual sf::Drawable *getSFMLDrawable() = 0;
        virtual sf::Transformable *getSFMLTransformable() = 0;

    protected:
        Kcoy::Transform *transformRef;
        unsigned int priority;
        unsigned int pixelsPerUnit;

        sf::Vector2f calculateOrigin(const sf::Vector2u &size, SpriteOrigin origin);
    };
}
