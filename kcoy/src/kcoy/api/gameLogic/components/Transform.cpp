#include "Transform.h"

Kcoy::Transform::Transform(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 position, Kcoy::Vector2 scale, float rotation) :
    isLocalDirty(false), isWorldDirty(false), shouldUpdateChildWorldValues(false), up(0, 1), right(1, 0), parentTransformRef(parentTransformRef),
    position(position), scale(scale), rotation(rotation) {
    if (parentTransformRef != nullptr) {
        parentTransformRef->addChildTransformRef(this);
    }
    calculateWorldValues();
}

Kcoy::Transform::Transform(Kcoy::Vector2 position, Kcoy::Vector2 scale, float rotation) :
    Kcoy::Transform(nullptr, position, scale, rotation) { }

Kcoy::Transform::Transform(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 position, Kcoy::Vector2 scale) :
    Kcoy::Transform(parentTransformRef, position, scale, 0) { }

Kcoy::Transform::Transform(Vector2 position, Vector2 scale) :
    Kcoy::Transform(nullptr, position, scale, 0) { }

Kcoy::Transform::Transform(Kcoy::Transform * parentTransformRef, Kcoy::Vector2 position, float rotation) :
    Kcoy::Transform(parentTransformRef, position, Kcoy::Vector2(1, 1), rotation) { }

Kcoy::Transform::Transform(Kcoy::Vector2 position, float rotation) :
    Kcoy::Transform(nullptr, position, Kcoy::Vector2(1, 1), 0) { }

Kcoy::Transform::Transform(Kcoy::Transform *parentTransformRef) :
    Kcoy::Transform(parentTransformRef, Kcoy::Vector2(), Kcoy::Vector2(1, 1), 0) { }

Kcoy::Transform::Transform() :
    Kcoy::Transform(nullptr, Kcoy::Vector2(), Kcoy::Vector2(1, 1), 0) { }

Kcoy::Transform::~Transform() { }


void Kcoy::Transform::setLocalPosition(Vector2 position)
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    this->position.setXY(position);
    isWorldDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
Kcoy::Vector2 Kcoy::Transform::getPosition()
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    return this->position;
}
void Kcoy::Transform::setWorldPosition(Kcoy::Vector2 worldPosition)
{
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    this->worldPosition.setXY(worldPosition);
    isLocalDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
Kcoy::Vector2 Kcoy::Transform::getWorldPosition()
{
    if (this->parentTransformRef != nullptr && this->parentTransformRef->getIsWorldDirty()) {
        this->parentTransformRef->calculateWorldValues();
    }
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    return this->worldPosition;
}

void Kcoy::Transform::setLocalScale(Vector2 scale)
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    this->scale.setXY(scale);
    isWorldDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
Kcoy::Vector2 Kcoy::Transform::getScale()
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    return this->scale;
}
void Kcoy::Transform::setWorldScale(Kcoy::Vector2 worldScale)
{
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    this->worldScale.setXY(worldScale);
    this->isLocalDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
Kcoy::Vector2 Kcoy::Transform::getWorldScale()
{
    if (this->parentTransformRef != nullptr && this->parentTransformRef->getIsWorldDirty()) {
        this->parentTransformRef->calculateWorldValues();
    }
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    return this->worldScale;
}

void Kcoy::Transform::setLocalRotation(float degrees)
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    this->rotation = degrees;
    isWorldDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
float Kcoy::Transform::getRotation()
{
    if (this->isLocalDirty) {
        this->calculateLocalValues();
    }
    return this->rotation;
}
void Kcoy::Transform::setWorldRotation(float worldDegrees)
{
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    this->worldRotation = worldDegrees;
    this->isLocalDirty = true;

    if (this->childTransformRefs.size() > 0) {
        this->shouldUpdateChildWorldValues = true;
    }
}
float Kcoy::Transform::getWorldRotation()
{
    if (this->parentTransformRef != nullptr && this->parentTransformRef->getIsWorldDirty()) {
        this->parentTransformRef->calculateWorldValues();
    }
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    return this->worldRotation;
}

Kcoy::Vector2 Kcoy::Transform::getUp()
{
    if (this->parentTransformRef != nullptr && this->parentTransformRef->getIsWorldDirty()) {
        this->parentTransformRef->calculateWorldValues();
    }
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    return this->up;
}

Kcoy::Vector2 Kcoy::Transform::getRight()
{
    if (this->parentTransformRef != nullptr && this->parentTransformRef->getIsWorldDirty()) {
        this->parentTransformRef->calculateWorldValues();
    }
    if (this->isWorldDirty) {
        this->calculateWorldValues();
    }
    return this->right;
}

bool Kcoy::Transform::getIsLocalDirty()
{
    return this->isLocalDirty;
}
bool Kcoy::Transform::getIsWorldDirty()
{
    return this->isWorldDirty;
}

void Kcoy::Transform::calculateLocalValues()
{
    if (this->parentTransformRef == nullptr) {
        this->position.setXY(this->worldPosition);
        this->scale.setXY(this->worldScale);
        this->rotation = this->worldRotation;
    }
    else {
        this->position = this->worldPosition - this->parentTransformRef->getWorldPosition();
        this->position.setXY(Kcoy::Vector2(
            this->position.dot(this->parentTransformRef->getRight()),
            this->position.dot(this->parentTransformRef->getUp())));
        
        this->scale.setXY(this->scale / this->parentTransformRef->getScale());

        this->rotation = this->worldRotation - this->parentTransformRef->getWorldRotation();
    }
    this->up.setXY(Kcoy::Vector2(-Kcoy::Math::sinR(this->worldRotation * Kcoy::Math::Deg2Rad), Kcoy::Math::cosR(this->worldRotation * Kcoy::Math::Deg2Rad)));
    this->right.setXY(Kcoy::Vector2(Kcoy::Math::cosR(this->worldRotation * Kcoy::Math::Deg2Rad), Kcoy::Math::sinR(this->worldRotation * Kcoy::Math::Deg2Rad)));

    this->isLocalDirty = false;
}

void Kcoy::Transform::calculateWorldValues()
{
    if (this->parentTransformRef == nullptr) {
        this->worldPosition.setXY(this->position);
        this->worldScale.setXY(this->scale);
        this->worldRotation = this->rotation;
    }
    else {
        Kcoy::Vector2 newWorldPos = this->position * parentTransformRef->getWorldScale();
        newWorldPos.setXY((parentTransformRef->getRight() * newWorldPos.getX()) +
            (parentTransformRef->getUp() * newWorldPos.getY()));
        newWorldPos += parentTransformRef->getWorldPosition();
        
        this->worldPosition.setXY(newWorldPos);

        this->worldScale.setXY(parentTransformRef->getWorldScale() * this->scale);
        
        this->worldRotation = parentTransformRef->getWorldRotation() + this->rotation;

    }
    this->up.setXY(Kcoy::Vector2(-Kcoy::Math::sinR(this->worldRotation * Kcoy::Math::Deg2Rad), Kcoy::Math::cosR(this->worldRotation * Kcoy::Math::Deg2Rad)));
    this->right.setXY(Kcoy::Vector2(Kcoy::Math::cosR(this->worldRotation * Kcoy::Math::Deg2Rad), Kcoy::Math::sinR(this->worldRotation * Kcoy::Math::Deg2Rad)));

    this->isWorldDirty = false;

    this->calculateChildWorldValues();
}

void Kcoy::Transform::calculateChildWorldValues()
{
    for (auto &transformRefPair : childTransformRefs) {
        transformRefPair.second->calculateWorldValues();
    }
    this->shouldUpdateChildWorldValues = false;
}

void Kcoy::Transform::addChildTransformRef(Transform *childTransformRef)
{
    this->childTransformRefs[childTransformRef] = childTransformRef;
}
