#include "BoxCollider.h"
#include "kcoy/engine/physics/PhysicsEngine.h"
#include "kcoy/engine/render/Render.h"

Kcoy::BoxCollider::BoxCollider(Kcoy::Transform *transformRef, Kcoy::RigidBody *rigidBodyRef, bool isSensor, float width, float height, float radius) {
    this->gameObjectRef = transformRef->gameObjectRef;
    cpBody *body;
    if (rigidBodyRef == NULL) {
        body = cpSpaceGetStaticBody(KcoyEngine::PhysicsEngine::Instance().getSpace());
    }
    else {
        body = rigidBodyRef->body;
    }
    this->shape = cpBoxShapeNew(body, width, height, radius);
    if (isSensor) {
        cpShapeSetSensor(this->shape, true);
    }
    KcoyEngine::PhysicsEngine::Instance().addShape(this, this->shape);

#ifdef KY_DEBUG
    this->transformRef = transformRef;
    this->gizmo = new Kcoy::BoxGizmo(this->transformRef, width, height);
#endif // KY_DEBUG
}


Kcoy::BoxCollider::~BoxCollider() {
#ifdef KY_DEBUG
    delete this->gizmo;
#endif // KY_DEBUG

    KcoyEngine::PhysicsEngine::Instance().removeCollisionStartCallback(this);
    KcoyEngine::PhysicsEngine::Instance().removeCollisionStayCallback(this);
    KcoyEngine::PhysicsEngine::Instance().removeCollisionEndCallback(this);
    KcoyEngine::PhysicsEngine::Instance().removeShape(this->shape);
}

void Kcoy::BoxCollider::addCollisionStartCallback(std::function<void(Kcoy::GameObject*)> f) {
    KcoyEngine::PhysicsEngine::Instance().addCollisionStartCallback(this, f);
}

void Kcoy::BoxCollider::addCollisionStayCallback(std::function<void(Kcoy::GameObject*)> f) {
    KcoyEngine::PhysicsEngine::Instance().addCollisionStayCallback(this, f);
}

void Kcoy::BoxCollider::addCollisionEndCallback(std::function<void(Kcoy::GameObject*)> f) {
    KcoyEngine::PhysicsEngine::Instance().addCollisionEndCallback(this, f);
}
