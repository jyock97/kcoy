#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/gameLogic/components/Sprite.h"
#include <string>

namespace Kcoy {
    class KCOY_API SpriteText : public Kcoy::Sprite {
    public:
        SpriteText(Kcoy::Transform *transformRef, int priority, std::string &text, int fontSize, Kcoy::SpriteOrigin origin);
        SpriteText(Kcoy::Transform *transformRef, int priority, std::string &text, int fontSize);
        ~SpriteText();

        void setText(std::string &text);

        sf::Drawable *getSFMLDrawable();
        sf::Transformable *getSFMLTransformable();

    private:
        std::string textStr;
        sf::Font font;
        sf::Text text;
    };
}
