#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/math/Vector2.h"
#include "kcoy/api/math/Math.h"
#include "kcoy/api/observer/Observer.h"
#include <unordered_map>

//TODO add rotation as well
namespace Kcoy {
    class GameObject;
    class KCOY_API Transform {
    public:
        Transform(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 position, Kcoy::Vector2 scale, float rotation);
        Transform(Kcoy::Vector2 position, Kcoy::Vector2 scale, float rotation);
        Transform(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 position, Kcoy::Vector2 scale);
        Transform(Kcoy::Vector2 position, Kcoy::Vector2 scale);
        Transform(Kcoy::Transform *parentTransformRef, Kcoy::Vector2 position, float rotation);
        Transform(Kcoy::Vector2 position, float rotation);
        Transform(Kcoy::Transform *parentTransformRef);
        Transform();
        ~Transform();

        Kcoy::GameObject *gameObjectRef;

        void setLocalPosition(Kcoy::Vector2 position);
        Kcoy::Vector2 getPosition();
        void setWorldPosition(Kcoy::Vector2 worldPosition);
        Kcoy::Vector2 getWorldPosition();
        
        void setLocalScale(Kcoy::Vector2 scale);
        Kcoy::Vector2 getScale();
        void setWorldScale(Kcoy::Vector2 worldScale);
        Kcoy::Vector2 getWorldScale();
        
        void setLocalRotation(float degrees);
        float getRotation();
        void setWorldRotation(float worldDegrees);
        float getWorldRotation();

        Kcoy::Vector2 getUp();
        Kcoy::Vector2 getRight();
        
        bool getIsLocalDirty();
        bool getIsWorldDirty();

    private:
        bool isLocalDirty;
        bool isWorldDirty;
        bool shouldUpdateChildWorldValues;
        Transform *parentTransformRef;
        std::unordered_map<Transform *, Transform *> childTransformRefs;

        Kcoy::Vector2 position;
        Kcoy::Vector2 worldPosition;
        Kcoy::Vector2 scale;
        Kcoy::Vector2 worldScale;
        float rotation;
        float worldRotation;
        Kcoy::Vector2 up;
        Kcoy::Vector2 right;


    protected:
        void addChildTransformRef(Transform *childTransformRef);
        
        void calculateLocalValues();
        void calculateWorldValues();
        void calculateChildWorldValues();
    };
}
