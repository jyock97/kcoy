#include "SpriteRender.h"
#include "kcoy/engine/render/Render.h"

Kcoy::SpriteRender::SpriteRender(Kcoy::Transform *transformRef, const std::string &filePath, const unsigned int priority, const unsigned int pixelsPerUnit) :
    Kcoy::Sprite(transformRef, priority, pixelsPerUnit), filePath(filePath) {
    this->filePath = filePath;
    if (!texture.loadFromFile(this->filePath)) {
        Kcoy::Logger::CoreLogger()->error("Error trying to texture from file:{}", this->filePath);
    }
    this->sprite = sf::Sprite(this->texture);
    
    this->sprite.setOrigin(calculateOrigin(this->texture.getSize(), SpriteOrigin::Center));
    this->sprite.setScale(sf::Vector2f(KcoyEngine::Render::PixelsPerUnit / pixelsPerUnit, KcoyEngine::Render::PixelsPerUnit / pixelsPerUnit));
    this->pixelsPerUnit = pixelsPerUnit;

    KcoyEngine::Render::Instance().add(this);

#ifdef KY_DEBUG
    this->gizmo = new Kcoy::CircleGizmo(this->transformRef, 8); //TODO calculate this value
#endif // KY_DEBUG
}
Kcoy::SpriteRender::SpriteRender(Kcoy::Transform *transformRef, const std::string & filePath, unsigned int priority, unsigned int pixelsPerUnit, SpriteOrigin origin) :
    Kcoy::SpriteRender(transformRef, filePath, priority, pixelsPerUnit) {
    this->sprite.setOrigin(calculateOrigin(this->texture.getSize(), origin));
}
Kcoy::SpriteRender::~SpriteRender() {
#ifdef KY_DEBUG
    delete this->gizmo;
#endif // KY_DEBUG
    KcoyEngine::Render::Instance().remove(this);
}

sf::Drawable *Kcoy::SpriteRender::getSFMLDrawable()
{
    return (sf::Drawable*) &this->sprite;
}

sf::Transformable *Kcoy::SpriteRender::getSFMLTransformable()
{
    return (sf::Transformable*) &this->sprite;
}
