#include "RigidBody.h"
#include "kcoy/engine/physics/PhysicsEngine.h"

Kcoy::RigidBody::RigidBody(Kcoy::Transform *transformRef, bool isStatic) {
    this->transformRef = transformRef;
    createBody(1, cpv(transformRef->getPosition().getX(), transformRef->getPosition().getY()), isStatic);
}

Kcoy::RigidBody::RigidBody(Kcoy::Transform *transformRef, cpFloat mass, bool isStatic) {
    this->transformRef = transformRef;
    createBody(mass, cpv(transformRef->getPosition().getX(), transformRef->getPosition().getY()), isStatic);
}

Kcoy::RigidBody::RigidBody(Kcoy::Transform *transformRef, cpFloat mass, cpVect pos, bool isStatic) {
    this->transformRef = transformRef;
    createBody(mass, pos, isStatic);
}

Kcoy::RigidBody::~RigidBody() {
    KcoyEngine::PhysicsEngine::Instance().removeBody(this->body);
    cpBodyFree(this->body);
}

Kcoy::Vector2 &Kcoy::RigidBody::getVelocity()
{
    cpVect v = cpBodyGetVelocity(this->body);
    this->velocity.setXY(v.x, v.y);

    return this->velocity;
}

void Kcoy::RigidBody::setVelocity(Kcoy::Vector2 velocity)
{
    cpBodySetVelocity(this->body, cpv(velocity.getX(), velocity.getY()));
    this->velocity.setXY(velocity);
}


void Kcoy::RigidBody::createBody(cpFloat mass, cpVect pos, bool isStatic) {
    if (isStatic) {
        this->body = cpBodyNewStatic();
    }
    else {
        this->body = cpBodyNew(mass, 1);
    }
    cpBodySetPosition(this->body, pos);
    KcoyEngine::PhysicsEngine::Instance().addBody(this->body, this->transformRef);
}
