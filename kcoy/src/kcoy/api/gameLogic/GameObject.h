#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/logger/Logger.h"
#include "kcoy/api/gameLogic/components/Transform.h"
#include <string>

namespace Kcoy {
    class KCOY_API GameObject {
    public:
        Kcoy::Transform transform;

        GameObject(const char *name);
        GameObject(const char *name, Kcoy::Transform *parentTransformRef);
        ~GameObject();

        std::string name;

        void enable();
        void disable();

        virtual void update() = 0;
    };
}
