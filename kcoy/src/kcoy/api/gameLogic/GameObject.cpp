#include "GameObject.h"
#include "kcoy/engine/gameLogic/GameObjectHandler.h"

Kcoy::GameObject::GameObject(const char *name, Kcoy::Transform *parentTransformRef) :
    transform(parentTransformRef), name(name)
{
    this->transform.gameObjectRef = this;
    this->enable();
}

Kcoy::GameObject::GameObject(const char *name) :
    Kcoy::GameObject(name, nullptr) {}

Kcoy::GameObject::~GameObject() {
    this->disable();
}

void Kcoy::GameObject::enable() {
    KcoyEngine::GameObjectHandler::Instance().add(this);
}

void Kcoy::GameObject::disable() {
    KcoyEngine::GameObjectHandler::Instance().remove(this);
}
