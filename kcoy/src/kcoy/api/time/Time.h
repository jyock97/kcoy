#pragma once
#include "kcoy/Core.h"

namespace Kcoy {
    class KCOY_API Time {
    public:
        static float deltaTime();
    };
}
