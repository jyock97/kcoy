#include "Time.h"
#include "kcoy/engine/time/TimeHandler.h"

float Kcoy::Time::deltaTime() {
    return KcoyEngine::TimeHandler::Instance().getDeltaTime();
}
