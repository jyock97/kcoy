#pragma once

#include "kcoy/Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace Kcoy {
    class KCOY_API Logger {
    public:
        static void init();
        static  std::shared_ptr<spdlog::logger> &CoreLogger();
        static std::shared_ptr<spdlog::logger> &ClientLogger();

    private:
        static std::shared_ptr<spdlog::logger> coreLogger;
        static std::shared_ptr<spdlog::logger> clientLogger;
    };
}
