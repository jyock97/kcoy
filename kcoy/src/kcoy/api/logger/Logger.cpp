#include "Logger.h"

std::shared_ptr<spdlog::logger> Kcoy::Logger::coreLogger = spdlog::stdout_color_mt("CORE");
std::shared_ptr<spdlog::logger> Kcoy::Logger::clientLogger = spdlog::stdout_color_mt("CLIENT");

std::shared_ptr<spdlog::logger>& Kcoy::Logger::CoreLogger() { return coreLogger; }

std::shared_ptr<spdlog::logger>& Kcoy::Logger::ClientLogger() { return clientLogger; }

void Kcoy::Logger::init() {
    spdlog::set_pattern("[%T:%p][%^%=6n%$] %v");
}
