#pragma once
#include "kcoy/Core.h"

namespace Kcoy {
    class KCOY_API ObserverFunction {
    public:
        virtual void update() = 0;
    };
}
