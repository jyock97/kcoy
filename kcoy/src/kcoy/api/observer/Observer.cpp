#include "Observer.h"

Kcoy::Observer::Observer() { }
Kcoy::Observer::~Observer() { }

void Kcoy::Observer::attach(Kcoy::ObserverFunction *observerFunction) {
    this->subscribers[observerFunction] = observerFunction;
}

void Kcoy::Observer::detach(ObserverFunction *observerFunction) {
    this->subscribers.erase(observerFunction);
}

void Kcoy::Observer::dispatch() {
    for (auto &[_, observerFunction] : subscribers) {
        observerFunction->update();
    }
}
