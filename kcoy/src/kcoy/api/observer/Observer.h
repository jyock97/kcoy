#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/observer/ObserverFunction.h"
#include <map>

namespace Kcoy {
    class KCOY_API Observer {
    public:
        Observer();
        ~Observer();

        void attach(ObserverFunction *observerFunction);
        void detach(ObserverFunction *observerFunction);

        void dispatch();

    private:
        std::map<ObserverFunction*, ObserverFunction*> subscribers;
    };
}
