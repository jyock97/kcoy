#include "CircleGizmo.h"
#include "kcoy/engine/render/Render.h"

Kcoy::CircleGizmo::CircleGizmo(Kcoy::Transform *transformRef, float radius) :
    Kcoy::Gizmo(transformRef)
{
    this->shape = new sf::CircleShape(radius);
    this->shape->setOutlineThickness(radius);
    this->shape->setFillColor(sf::Color::White);
    this->shape->setOutlineColor(sf::Color::Black);
    this->shape->setOrigin(radius, radius);

    KcoyEngine::Render::Instance().add(this);
}

Kcoy::CircleGizmo::~CircleGizmo() {
    KcoyEngine::Render::Instance().remove(this);
    delete(this->shape);
}
