#pragma once
#include "kcoy/api/gizmos/Gizmo.h"

namespace Kcoy {
    class BoxGizmo : public Gizmo {
    public:
        BoxGizmo(Kcoy::Transform *transformRef, float width, float height);
        ~BoxGizmo();
    };
}
