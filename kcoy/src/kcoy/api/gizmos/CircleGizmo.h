#pragma once
#include "kcoy/api/gizmos/Gizmo.h"

namespace Kcoy {
    class CircleGizmo : public Gizmo {
    public:
        CircleGizmo(Kcoy::Transform *transformRef, float radius);
        ~CircleGizmo();
    };
}
