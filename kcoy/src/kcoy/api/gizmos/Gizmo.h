#include "kcoy/api/gameLogic/components/Transform.h"
#include <unordered_map>
#include <SFML/Graphics.hpp>
#pragma once

namespace Kcoy {
    class Gizmo {
    public:
        Gizmo(Kcoy::Transform *transformRef);
        ~Gizmo();

        Kcoy::Transform *getTransformRef();
        sf::Shape *getShape();

    protected:
        sf::Shape *shape;
    private:
        Kcoy::Transform *transformRef;
    };
}
