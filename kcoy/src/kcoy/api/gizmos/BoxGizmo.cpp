#include "BoxGizmo.h"
#include "kcoy/engine/render/Render.h"

Kcoy::BoxGizmo::BoxGizmo(Kcoy::Transform *transformRef, float width, float height) :
    Kcoy::Gizmo(transformRef)
{
    int boxThiknes = 2; // TODO calculate this base on the camera zoom
    sf::Vector2f size(width, height);
    size *= (float) KcoyEngine::Render::PixelsPerUnit;
    size -= sf::Vector2f((boxThiknes * 2), (boxThiknes * 2));
    this->shape = new sf::RectangleShape(size);
    this->shape->setOutlineThickness(boxThiknes);
    this->shape->setFillColor(sf::Color::Transparent);
    this->shape->setOutlineColor(sf::Color::Green);
    this->shape->setOrigin(size.x / 2, size.y / 2);

    KcoyEngine::Render::Instance().add(this);
}

Kcoy::BoxGizmo::~BoxGizmo() {
    KcoyEngine::Render::Instance().remove(this);
    delete(this->shape);
}
