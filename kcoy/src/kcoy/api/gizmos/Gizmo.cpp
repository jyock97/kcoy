#include "Gizmo.h"

Kcoy::Gizmo::Gizmo(Kcoy::Transform * transformRef)
{
    this->transformRef = transformRef;
}

Kcoy::Gizmo::~Gizmo() {}

Kcoy::Transform *Kcoy::Gizmo::getTransformRef()
{
    return this->transformRef;
}

sf::Shape * Kcoy::Gizmo::getShape()
{
    return this->shape;
}
