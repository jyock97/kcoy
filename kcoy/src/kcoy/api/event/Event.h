#pragma once
#include "kcoy/Core.h"
#include "kcoy/api/event/EventEnums.h"

namespace Kcoy {
    class KCOY_API Event {
    public:
        /* Return true or false if the window event was triggered in the current frame */
        static bool windowEvent(Kcoy::WindowEvent eventType);

        static bool keyPressed(Kcoy::KeyCode keyCode);
        static bool keyHold(Kcoy::KeyCode keyCode);
        static bool keyReleased(Kcoy::KeyCode keyCode);
    };
}
