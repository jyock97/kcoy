#include "Event.h"
#include "kcoy/engine/event/EventHandler.h"

bool Kcoy::Event::windowEvent(Kcoy::WindowEvent eventType) {
    return KcoyEngine::EventHandler::Instance().isWindowEventSet(eventType);
}

bool Kcoy::Event::keyPressed(Kcoy::KeyCode keyCode) {
    return KcoyEngine::EventHandler::Instance().isKeyPressedSet(keyCode);
}
bool Kcoy::Event::keyHold(Kcoy::KeyCode keyCode) {
    return KcoyEngine::EventHandler::Instance().isKeyHoldSet(keyCode);
}
bool Kcoy::Event::keyReleased(Kcoy::KeyCode keyCode) {
    return KcoyEngine::EventHandler::Instance().isKeyReleasedSet(keyCode);
}
