project "kcoy"

    kind "SharedLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "On"

    targetdir ("%{wks.location}/bin/" .. outputDir .. "/%{prj.name}")
    objdir ("%{wks.location}/bin-int/" .. outputDir .. "/%{prj.name}")

    files {
        "src/**.h",
        "src/**.cpp"
    }

    includedirs {
        "%{wks.location}/kcoy/src",
        "vendor/*/include",
    }

    libdirs {
    }

    links {
        "chipmunk"
    }

    filter "system:windows"
        systemversion "latest"
        defines {
            "KY_PLATFORM_WINDOWS",
            "KY_BUILD_DLL"
        }

        includedirs {
            "vendor/*/win_64/include"
        }

        libdirs {
            "vendor/*/win_64/lib"
        }

        links {
            -- sfml dependencies
            "freetype",
            "opengl32",
            "gdi32",
            "winmm"
        }

        postbuildcommands {
            "{COPY} %{wks.location}/kcoy/vendor/sfml/win_64/bin/ %{wks.location}bin/" .. outputDir .."/" .. projectName .. "/", -- TODO need to remove debug dlls here
            "{COPY} %{wks.location}/bin/" .. outputDir .."/kcoy/kcoy.dll %{wks.location}bin/" .. outputDir .."/" .. projectName .. "/"
        }

    filter "system:linux"
        includedirs {
            "vendor/*/linux_64/include"
        }
        libdirs {
            "vendor/*/linux_64/lib"
        }
        links {
            "sfml-graphics",
            "sfml-window",
            "sfml-system"
        }

    filter "configurations:Debug"
        defines "KY_DEBUG"
        runtime "Debug"
        symbols "on"

        libdirs {
            "vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/DebugDLL"
        }


    filter "configurations:Release"
        defines "KY_RELEASE"
        runtime "Release"
        optimize "on"

        libdirs {
            "vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/ReleaseDLL"
        }

    filter "configurations:Dist"
        defines "KY_DIST"
        runtime "Release"
        optimize "on"

        libdirs {
            "vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/ReleaseDLL"
        }

    filter {"system:windows", "configurations:Debug"}
        links {
            "sfml-graphics-d",
            "sfml-window-d",
            "sfml-system-d"
        }

        postbuildcommands {
            "{COPY} %{wks.location}/kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/DebugDLL/chipmunk.dll %{wks.location}bin/" .. outputDir .."/" .. projectName .. "/"
        }

    filter {"system:windows", "configurations:not Debug"}
        links {
            "sfml-graphics",
            "sfml-window",
            "sfml-system"
        }

        postbuildcommands {
            "{COPY} %{wks.location}/kcoy/vendor/Chipmunk2D/msvc/VS2015/chipmunk/x64/ReleaseDLL/chipmunk.dll %{wks.location}bin/" .. outputDir .."/" .. projectName .. "/"
        }
